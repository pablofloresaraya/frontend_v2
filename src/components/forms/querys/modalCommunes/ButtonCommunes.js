import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';

import ModalSearchCommuneType from './ModalSearchCommuneType'; 

export function ButtonCommunes(props){    
  
    function _handleCloseModal(){
        $(document.getElementById(props.modalId)).modal("hide")
    }
    
    function _handleShow(){
        console.log("modal: "+props.modalId)
        ReactDOM.render(
            <ModalSearchCommuneType 
                id={props.modalId}
                setValue={props.setValue}
                provincia={props.provincia}
                close={_handleCloseModal}
            />,
            document.getElementById("section_modal"),
            function(){
                $(document.getElementById(props.modalId)).modal("show")
            }
        );
    }  

    return(
        <button className="btn btn-outline-info btn-sm" type="button" onClick={_handleShow} disabled={props.provincia===""}>
            <i className="fas fa-list"></i>
        </button>
    );
    
}

export default ButtonCommunes;