import React, {Component} from 'react';

import ButtonSearchPatient from '../querys/modalsearchpatient/buttonsearchpatient'

export class Form3 extends Component{

    constructor(props){
        super(props);

        this.state = {
            paciente: {pc_cod: "", pc_nombre: "", pc_apellido: ""},
        }

        this._setPaciente = this._setPaciente.bind(this);
    }

    _setPaciente(p_data){
        this.setState(
            {
                paciente: {pc_cod: p_data.pc_cod, pc_nombre: p_data.pc_nombre, pc_apellido: p_data.pc_apellido},
            }
        )
    }

    _handleChangePcCod(event) {
        this.setState({paciente: {pc_cod: event.target.value}});
    }

    _handleChangePcNombre(event) {
        this.setState({paciente: {pc_nombre: event.target.value}});
    }

    _handleChangePcApellido(event) {
        this.setState({paciente: {pc_apellido: event.target.value}});
    }

    render(){
        return(
        <div className="tab-content" id="myTabAppOpenContent" style={{height: '30rem',}}>
            <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <div className="row">
                <div className="col-lg-12">
                    <div className="card shadow-lg border-0 rounded-lg mt-1">
                        <div className="card-body">
                            <form>
                                <div className="form-row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="small mb-1" htmlFor="inputFirstName">Paciente</label>
                                                <div className="input-group input-group-xs">
                                                    <input className="form-control py-1 col-md-2" 
                                                        id="inputRut" 
                                                        type="text" 
                                                        placeholder="Ingrese rut del paciente"
                                                        value={this.state.paciente.pc_cod}
                                                        onChange={this._handleChangePcCod.bind(this)} 
                                                    />
                                                    <input className="form-control py-1" 
                                                        id="inputFirstName" 
                                                        type="text" 
                                                        placeholder=""
                                                        value={this.state.paciente.pc_nombre}
                                                        onChange={this._handleChangePcNombre.bind(this)}   
                                                    />
                                                    <div className="input-group-append">
                                                        <ButtonSearchPatient 
                                                            modalId="cnPaciente"
                                                            setValue={this._setPaciente}/>
                                                    </div>
                                                </div>
                                          </div>
                                      </div>
                                      <div className="col-md-6">
                                          <div className="form-group">
                                              <label className="small mb-1" htmlFor="inputLastName">Apellido</label>
                                                    <input className="form-control form-control-xs py-1" 
                                                        id="inputLastName" 
                                                        type="text" 
                                                        placeholder=""
                                                        value={this.state.paciente.pc_apellido}
                                                        onChange={this._handleChangePcApellido.bind(this)}   
                                                    />
                                          </div>
                                      </div>
                                  </div>
                                  <div className="form-group">
                                      <label className="small mb-1" htmlFor="inputEmailAddress">Email</label>
                                      <input className="form-control form-control-xs py-1" id="inputEmailAddress" type="email" aria-describedby="emailHelp" placeholder="Enter email address" />
                                  </div>
                                  <div className="form-row">
                                      <div className="col-md-6">
                                          <div className="form-group">
                                              <label className="small mb-1" htmlFor="inputPassword">Password</label>
                                              <input className="form-control form-control-sm py-1" id="inputPassword" type="password" placeholder="Enter password" />
                                          </div>
                                      </div>
                                      <div className="col-md-6">
                                          <div className="form-group">
                                              <label className="small mb-1" htmlFor="inputConfirmPassword">Confirm Password</label>
                                              <input className="form-control form-control-sm py-1" id="inputConfirmPassword" type="password" placeholder="Confirm password" />
                                          </div>
                                      </div>
                                  </div>
                                  <div className="form-group mt-4 mb-0">
                                      <a className="btn btn-primary btn-block" href="login.html">Create Account</a>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
            <div className="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">Aplicacion 1</div>
            <div className="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">Aplicacion 2</div>           
        </div>
      );
    }
  }