import React from 'react';

export function NavItem(props){
  return(
      <li className="nav-item dropdown">{ props.children }</li>
  )
}