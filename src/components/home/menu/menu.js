import React, {Component} from 'react';
//import { menuService } from '../../_services';

import Section from './section/section.js'
import Module from './section/module/module.js'

export class Menu extends Component{ 
    /*
    constructor(props){
        super(props);
    }
    */
    render(){
        console.log("2.1. Menu props: ",this.props);
        return(
            <div id="layoutSidenav_nav">
                <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div className="sb-sidenav-menu">
                        <div className="nav">

                            <Section sectionName="Inicio" />
                                <Module moduleId="0" moduleActive={this.props.menuModuleActive} onClick={this.props._handleOnClickModule} moduleName="Favoritos" moduleUrl="#" />

                            <Section sectionName="Modulos" />
                                <Module moduleId="1" moduleActive={this.props.menuModuleActive} onClick={this.props._handleOnClickModule} moduleName="Cl&iacute;nica" moduleUrl="#" />
                                <Module moduleId="2" moduleActive={this.props.menuModuleActive} onClick={this.props._handleOnClickModule} moduleName="Finanzas" moduleUrl="#" />
                                <Module moduleId="5" moduleActive={this.props.menuModuleActive} onClick={this.props._handleOnClickModule} moduleName="Cuentas por Pagar" moduleUrl="#" />
                                <Module moduleId="6" moduleActive={this.props.menuModuleActive} onClick={this.props._handleOnClickModule} moduleName="Tesorer&iacute;a" moduleUrl="#" />
                                <Module moduleId="3" moduleActive={this.props.menuModuleActive} onClick={this.props._handleOnClickModule} moduleName="Inmobiliar&iacute;a" moduleUrl="#" />
                                <Module moduleId="4" moduleActive={this.props.menuModuleActive} onClick={this.props._handleOnClickModule} moduleName="Contabilidad" moduleUrl="#" />

                            <Section sectionName="Configuraci&oacute;n" />
                                <Module moduleId="7" moduleActive={this.props.menuModuleActive} onClick={this.props._handleOnClickModule} moduleName="Compa&ntilde;&iacute;a" moduleUrl="#" />
                                <Module moduleId="8" moduleActive={this.props.menuModuleActive} onClick={this.props._handleOnClickModule} moduleName="Otros" moduleUrl="#" />

                        </div>
                    </div>
                </nav>
            </div>
            );
    }
};