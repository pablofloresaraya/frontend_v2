import React, {Component} from 'react';
//import { connect } from 'react-redux';
//import { menuActions } from '../../../../_actions';
//import { menuService } from '../../../../_services';

export class Module extends Component{ 

    constructor(props){
        super(props);
        this._handleOnClick = this._handleOnClick.bind(this);
    }

    _handleOnClick(e) {
        this.props.onClick(this.props.moduleId, this.props.moduleName);
    }

    render(){
        return(
            
            <a className={(this.props.moduleActive===this.props.moduleId) ? 'nav-link active' : 'nav-link'}  
                href="/#" 
                onClick={(e) => {this.props.onClick(this.props.moduleId, this.props.moduleName)}} > 
                <div className="sb-nav-link-icon">
                    <i className="fas fa-tachometer-alt"></i>
                </div>
                {this.props.moduleName}
            </a>
        );
    }
    
}

export default Module;