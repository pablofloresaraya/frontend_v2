import React, {Component} from 'react'; 

export class PatientList extends Component{

    constructor(props){
        super(props);
        this.state = {patients: {}, Data: [], nombre: '', apellido: '', timeout: null};
        this._handleClick = this._handleClick.bind(this);
        this._handleOnKeyUp = this._handleOnKeyUp.bind(this);
        this.getPatients = this.getPatients.bind(this);
    }

    getPatients(){

        fetch('http://nefo:8082/patients?limit=10&nombre='+this.state.nombre+'&apellido='+this.state.apellido)
            .then(results => {
                return results.json();
            })
            .then(results => {
                let patients = results.data;
                if(patients!=undefined){
                    this.setState({Data: patients});
                }
            })
    }

    //componentDidMount(){
    //    this.getPatients();
    //}

    _handleClick(e,pac){

        fetch('http://nefo:8082/patients/'+pac.pc_cod)
            .then(results => {
                return results.json();
            })
            .then(results => {
                let data = results.data[0];
                if(data!=undefined){
                    this.props.setValue(data);
                    this.props.close();
                }
            })        
    }

    _handleOnKeyUp = (event) => {
        clearTimeout(this.state.timeout)
        this.setState({
            timeout: setTimeout(() => {
                this.getPatients();
                clearTimeout(this.state.timeout);
            },800)
        })      
    }

    _handleOnChangeNombre(event){
        this.setState({nombre: event.target.value})
    }

    _handleOnChangeApellido(event){
        this.setState({apellido: event.target.value})
    }

    render(){

        return(
            <div>
                <form className="ml-3"> 
                    <div className="form-group row">
                        <label htmlFor="pc_nombre" className="col-sm-2 col-form-label">Nombre</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control form-control-xs" id="pc_nombre" 
                                value={this.state.nombre}
                                onChange={this._handleOnChangeNombre.bind(this)}
                                onKeyUp={this._handleOnKeyUp} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="pc_apellido" className="col-sm-2 col-form-label">Apellido</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control form-control-xs" id="pc_apellido"
                                value={this.state.apellido}
                                onChange={this._handleOnChangeApellido.bind(this)}
                                onKeyUp={this._handleOnKeyUp} />
                        </div>
                    </div>
                </form>
                <div className="table-responsive">
                    <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellido</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.Data.map((patient, index) => {
                                return(  
                                    <tr key={patient.pc_cod} onClick={(e) => this._handleClick(e, patient)}>
                                        <td>{patient.pc_nombre}</td>
                                        <td>{patient.pc_apellido}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
};

export default PatientList; 