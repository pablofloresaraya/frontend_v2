import React, {Component} from 'react'; 
import { config } from '../../../_constants';
import {authHeader, base64EncodeUnicode} from '../../../_helpers';

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

export class RegionTypeList extends Component{

    constructor(props){
        super(props);
        
        this.state = {rows: {}, Data: [], filtro1: '', timeout: null};
        this._handleClick = this._handleClick.bind(this);
        this._handleOnKeyUp = this._handleOnKeyUp.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData(){
        
        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("territorial");
        let v_option = base64EncodeUnicode("1");
        
        const requestOptions = { 
            method: 'GET',
        //body: formData //JSON.stringify({ title: 'React POST Request Example' })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const resp = await response.json();                      
            this.setState({Data:resp.data});
        })
        .catch((error) => {
            console.error('Error:', error);
        });

    }

    _handleClick(e,row){
        this.props.setValue(row);
        this.props.close();
    }

    _handleOnKeyUp = (event) => {
        console.log("_handleOnKeyUp")
        clearTimeout(this.state.timeout)
        this.setState({
            timeout: setTimeout(() => {
                this.getData();
                clearTimeout(this.state.timeout);
            },800)
        })      
    }

    _handleOnChangeFiltro1(event){
        this.setState({filtro1: event.target.value})
    }

    componentDidMount(prevProps) {
        console.log("componentDidUpdate")
        // Uso tipico (no olvides de comparar los props):
        //if (this.props.userID !== prevProps.userID) {
          this.getData();
        //}
      }

    render(){

        const { SearchBar, ClearSearchButton } = Search;

        const columns = [
            {
                dataField: 'id',
                text: '#',
                id:'id',
                headerAlign: 'center',
                headerStyle: () => {
                    return { width: "15%" };
                }
            }, {
                dataField: 'name',
                text: 'Nombre',
                headerAlign: 'center'                          
            }
        ];

        const pagination = paginationFactory({
            page: 1,
            sizePerPage: 10,
            lastPageText: '>>',
            firstPageText: '<<',
            nextPageText: '>',
            prePageText: '<',
            showTotal: false,
            alwaysShowAllBtns: true,
            onPageChange: function (page, sizePerPage) {
              console.log('page', page);
              console.log('sizePerPage', sizePerPage);
            },
            onSizePerPageChange: function (page, sizePerPage) {
              console.log('page', page);
              console.log('sizePerPage', sizePerPage);
            }
        });

        const rowEvents = {
            onClick: (e, row, rowIndex) => {
                this.props.setValue(row);
                this.props.close();
            }
        };

        return(
        <>    
            <style type="text/css">
                {`
                .btn-secondary{
                    color: black;
                    background-color: white;
                    border-color: #dee2e6;
                }
        
                .btn-secondary:hover{
                    color: black;
                    background-color: white;
                    border-color: #dee2e6;
                }
        
                .btn-secondary.dropdown-toggle:focus{
                    color: black;
                    background-color: white;
                    border-color: #dee2e6;
                }
                .react-bs-table th {
                    vertical-align: center;
                }
                `}
            </style> 

            <div>
                <ToolkitProvider
                    bootstrap4
                    keyField='id'
                    data={this.state.Data}
                    columns={columns}
                    search
                >
                    {
                        props => (
                            <div>
                                <div className="text-right">    
                                    <SearchBar {...props.searchProps} placeholder="Buscar" />                                                               
                                </div>
                                <BootstrapTable
                                    rowEvents={rowEvents}
                                    rowStyle={{cursor:'pointer'}}
                                    pagination={pagination}
                                    {...props.baseProps}
                                />
                            </div>                                 
                        )
                    }
                </ToolkitProvider>  
            </div>
        </>    
        )
    }
};

export default RegionTypeList;