import React, {Component} from 'react';
import { config } from '../../_constants';
import { base64EncodeUnicode } from '../../_helpers';
import {ButtonAccounts} from '../querys/modalAccounts/ButtonAccounts';
import { Form } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import {MenuForm} from '../menuform/menuform';
import "./css/customDatePickerWidth.css";
import es from 'date-fns/locale/es';
registerLocale('es', es)

export class CgFrmLibroMayor extends Component{

    constructor(props){ 

        super(props);

        let v_cuenta = 1;
        let v_usuario = 1;

        this.state = {
            cuenta: {id:"",name:""}
            ,radio: false            
            ,fechaini: ""
            ,fechafin: ""
            ,fechaperiodo:null
            ,disfecha:true
            ,disperiodo:true
            ,cuenta_gen: v_cuenta
            ,user: v_usuario   
        }

        this.handleChangeRad = this.handleChangeRad.bind(this);
        this.handleChangeFechas = this.handleChangeFechas.bind(this);
        this.handleChangePeriodo = this.handleChangePeriodo.bind(this);
        this.handleReport = this.handleReport.bind(this);
        this.handleSetAccount = this.handleSetAccount.bind(this);
        this.handleReportPdf = this.handleReportPdf.bind(this);
        this.handleReportExcel = this.handleReportExcel.bind(this);
    }

    handleChangeRad(event){

        const { name, value } = event.target;

        this.setState({            
            ...this.state
            ,[name]:value                     
        });
        
        (this.state.radio==="F")
        ? this.setState({ fechaini: "", fechafin: "" })
        : this.setState({ fechaperiodo: null });
        

    }

    handleChangeFechas(event){

        const { name, value } = event.target;

        this.setState({            
            ...this.state.form,
            [name] : value                         
        })

    }

    handleChangePeriodo(fecha){
        this.setState({ fechaperiodo: fecha });
    }

    formatDate = (d) => {

        let month = d.getMonth();
        let day = d.getDate();
        month = month + 1;
        month = month + "";
        if (month.length === 1)
        {
            month = "0" + month;
        }
        day = day + "";
        if (day.length === 1)
        {
            day = "0" + day;
        }
        
        return day + '-' + month + '-' + d.getFullYear();
    
    };

    formatDateMonth = (d) => {

        let month = d.getMonth();

        month = month + 1;
        month = month + "";
        if (month.length === 1)
        {
            month = "0" + month;
        }

        return month + '-' + d.getFullYear();
    
    };

    handleReport(){        

        alert("en desarrolo");    
        
    }

    handleSetAccount(p_row) {
        console.log(JSON.stringify(p_row));        
        //add cuenta
        this.setState({
            cuenta: {
                ...this.state.cuenta,
                id  : p_row.id,
                name: p_row.name
            }             
        }) 

    }

    handleReportExcel(){
        //window.open(`${config.apiUrl}/accReportExcel.php`);
        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("8");
        
        const requestOptions = { 
            method: 'POST',
            body:  JSON.stringify({ 
                                    opcion: "E",
                                    cuenta:this.state.cuenta.id,
                                    fechaini:this.state.fechaini,
                                    fechafin:this.state.fechafin,
                                    periodo:this.state.fechaperiodo,
                                    cuentagen:this.state.cuenta_gen 
                                })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const data = await response.json();            
            (!data) ? this.handleShowModal() : window.open(config.apiUrl+data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
        
    }

    handleReportPdf(){

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("8");
        
        const requestOptions = { 
            method: 'POST',
            body:  JSON.stringify({ 
                                    opcion: "P",
                                    cuenta:this.state.cuenta.id,
                                    fechaini:this.state.fechaini,
                                    fechafin:this.state.fechafin,
                                    periodo:this.state.fechaperiodo,
                                    cuentagen:this.state.cuenta_gen  
                                })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const data = await response.json();            
            (!data) ? this.handleShowModal() : window.open(config.apiUrl+data); 
        })
        .catch((error) => {
            console.error('Error:', error);
        });

    }

    render(){

        const menuitems = [ 
            {type: 'PDF', onClick: this.handleReportPdf},            
            {type: 'EXCEL', onClick: this.handleReportExcel},
            {type: 'CERRAR', onClick: null}];

        return(            
            <div className="row" >
                <div className="col-lg-12">
                    <MenuForm menuitems={menuitems} />
                    <div className="card">
                        <div className="card-header">
                            <strong>Par&aacute;metros del Informe</strong>
                        </div>
                        <div className="card-body">
                            <form name="libroForm" className="form-horizontal" style={{marginTop:"1rem"}}> 
                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label control-label input-label text-right">Cuenta</label>
                                    <div className="col-md-2 col-lg-3">
                                        <div className="input-group input-group-xs">                                                                                                                   
                                            <input name="cuenta" type="text" className="form-control form-control-xs text-center" value={this.state.cuenta.name} disabled />                                       
                                            <div className="input-group-append">
                                                <ButtonAccounts
                                                    modalId="cnAccountType"
                                                    setValue={this.handleSetAccount}
                                                />
                                            </div>
                                        </div> 
                                    </div>
                                </div>    
                                <div className="form-group row">
                                    <div className="col-md-1 col-lg-2">
                                        &nbsp;
                                    </div>
                                    <div className="col-md-1 col-lg-3">
                                        <div className="row">
                                            <div className="col-md-1 col-lg-6">
                                                <input name="radio" id="fecha" type="radio" value="F" checked={this.state.radio==="F"} onChange={this.handleChangeRad}  />&nbsp;
                                                <label className="form-check-label col-form-label control-label input-label" htmlFor="fecha">
                                                    Por fecha
                                                </label>
                                            </div>
                                            <div className="col-md-1 col-lg-6">                                       
                                                <input name="radio" id="periodo" type="radio" value="P" checked={this.state.radio==="P"} onChange={this.handleChangeRad} />&nbsp;
                                                <label className="form-check-label col-form-label control-label input-label" htmlFor="periodo">
                                                    Por per&iacute;odo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Fecha Inicio</label>
                                    <div className="col-md-2 col-lg-3">
                                        <Form.Control name="fechaini" type="date" className="form-control form-control-xs" value={this.state.fechaini} onChange={this.handleChangeFechas} disabled={this.state.radio!=="F"} />
                                    </div>    
                                    <label className="col-md-1 col-lg-2 col-form-label-xs control-label input-label text-right">Fecha Termino</label>        
                                    <div className="col-md-2 col-lg-3">
                                        <Form.Control name="fechafin" type="date" className="form-control form-control-xs" value={this.state.fechafin} onChange={this.handleChangeFechas} disabled={this.state.radio!=="F"} />
                                    </div>  
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-1 col-lg-2 col-form-label control-label input-label text-right">Per&iacute;odo</label>
                                    <div className="col-md-2 col-lg-3 customDatePickerWidth">
                                        <DatePicker
                                            name="fechaperiodo"
                                            locale={'es'}                                      
                                            selected={this.state.fechaperiodo}
                                            onChange={(date) => this.handleChangePeriodo(date)}
                                            dateFormat="MM-yyyy"
                                            showMonthYearPicker
                                            className="form-control form-control-xs"
                                            isClearable
                                            disabled={this.state.radio!=="P"}                                                                               
                                        />
                                    </div>
                                </div>
                            </form>
                        </div>
                        &nbsp;    
                    </div>  
                </div>     
            </div>            
        )}

}

