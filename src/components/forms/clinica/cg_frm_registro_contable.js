import React, {Component} from 'react';

//import ButtonSearchPatient from '../querys/modalsearchpatient/buttonsearchpatient'

export class cgFrmRegistroContable extends Component{

    constructor(props){
        super(props);

        this.state = {
            general: {numero: "", fecha: "", tipo_comprobante: "", glosa: "", total_debe: 0, total_haber: 0},
            detalle: {numero: "", linea: "", cuenta: "", debe: 0, haber: 0},
            lineaDetalle: {numero: "", linea: "", cuenta: "", rut: "", detalle: ""},
        }

        //this._setPaciente = this._setPaciente.bind(this);
    }
    /*
    _setPaciente(p_data){
        this.setState(
            {
                paciente: {pc_cod: p_data.pc_cod, pc_nombre: p_data.pc_nombre, pc_apellido: p_data.pc_apellido},
            }
        )
    }
    */
    _handleChangeNumero(event) {
        this.setState({general: {numero: event.target.value}});
    }

    _handleChangeFecha(event) {
        this.setState({general: {fecha: event.target.value}});
    }

    _handleChangeTipoComprobante(event) {
        this.setState({general: {tipo_comprobante: event.target.value}});
    }

    _handleChangeGlosa(event) {
        this.setState({general: {glosa: event.target.value}});
    }

    render(){
        return(
        <div>
            <div class="card">
                <div class="card-header">
                    Informaci&oacute; General
                </div>
                <div class="card-body">                    
                    <form>
                        <div className="form-row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="small mb-1" htmlFor="number">N&uacute;mero</label>
                                    <input className="form-control py-1 col-md-2" 
                                        id="inputNumero" 
                                        type="text" 
                                        placeholder=""
                                        value={this.state.general.numero}
                                        onChange={this._handleChangeNumero.bind(this)} 
                                    />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="small" htmlFor="inputLastName">{this.state.general.nmb_usuario}</label>
                                </div>
                            </div>
                        </div>                
                        <div className="form-row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="small mb-1" htmlFor="number">Fecha</label>
                                    <input className="form-control py-1 col-md-2" 
                                        id="inputFecha" 
                                        type="text" 
                                        placeholder=""
                                        value={this.state.general.fecha}
                                        onChange={this._handleChangeFecha.bind(this)} 
                                    />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label className="small mb-1" htmlFor="number">Tipo Comprobante</label>
                                    <input className="form-control py-1 col-md-2" 
                                        id="inputTipoComprobante" 
                                        type="text" 
                                        placeholder=""
                                        value={this.state.general.tipo_comprobante}
                                        onChange={this._handleChangeTipoComprobante.bind(this)} 
                                    />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      );
    }
  }