import React, {Component} from 'react';

export class Form extends Component{
    /*
    constructor(props){
      super(props);
    }
    */
    render(){
        console.log("Form props: ", this.props);
        
      return(
        <>
            <div 
                    className={(this.props.applicationActive===this.props.id) ? 'tab-pane fade show active' : 'tab-pane fade show'}
                    id={this.props.id} 
                    role="tabpanel" 
                    aria-labelledby={"form"+this.props.id+"-tab"}
            >
                {this.props.content}
            </div>
        </>
      );
    }
  }