import React from 'react';

export function MenuFormItem(props){
    function _handleOnMouseEnter(e){
        props.setStateHover(true,props.index);
    }
    function _handleOnMouseLeave(e){
        props.setStateHover(false,props.index);
    }
    function setClassNameA(){
        //let v_clase='';
        //if(props.type==='NUEVO') v_clase = !props.disabled ? 'btn btn-outline-info btn-xs' : 'btn btn-outline-secondary btn-xs disabled';
        //else if(props.type==='GUARDAR') v_clase = !props.disabled ? 'btn btn-outline-info btn-xs' : 'btn btn-outline-secondary btn-xs disabled';
        //else if(props.type==='CONSULTAR') v_clase = !props.disabled ? 'btn btn-outline-info btn-xs' : 'btn btn-outline-secondary btn-xs disabled';
        //else if(props.type==='ANULAR') v_clase = !props.disabled ? 'btn btn-outline-info btn-xs' : 'btn btn-outline-secondary btn-xs disabled';
        //else if(props.type==='ELIMINAR') v_clase = !props.disabled ? 'btn btn-outline-info btn-xs' : 'btn btn-outline-secondary btn-xs disabled';
        let v_clase=!props.disabled ? 'btn btn-outline-info btn-xs' : 'btn btn-outline-secondary btn-xs disabled';
        return v_clase;
    }
    function setClassNameI(){
        let v_clase='';
        if(props.type==='NUEVO') v_clase = 'fa fa-file';
        else if(props.type==='GUARDAR') v_clase = 'fa fa-save';
        else if(props.type==='CONSULTAR') v_clase = 'fa fa-search';
        else if(props.type==='ANULAR') v_clase = 'fa fa-times';
        else if(props.type==='ELIMINAR') v_clase = 'fa fa-trash';
        else if(props.type==='PDF') v_clase = 'fas fa-file-pdf';
        else if(props.type==='EXCEL') v_clase = 'fas fa-file-excel';
        else if(props.type==='CERRAR') v_clase = 'fas fa-door-closed';

        return v_clase;
    }
    
    function styleMenuFormItem(){

        let style_item = {
            borderRadius: '0rem',
            backgroundColor: null,
            marginLeft: '5px',
        };
        
        return style_item;
    }

    function _handleOnClick(event){
        props.onClick(props.type);
    }

    return(
        <a className={setClassNameA()} href="/#" 
            style={styleMenuFormItem()} 
            onMouseEnter={(e) => _handleOnMouseEnter(e)}
            onMouseLeave={(e) => _handleOnMouseLeave(e)}
            onClick={(e) => _handleOnClick(e)}
        >
            <i className={setClassNameI()} title={props.title}></i>&nbsp;{props.title}
        </a>
    ) 
}