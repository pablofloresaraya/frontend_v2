import React from 'react';

//import { FormInicio } from './forminicio';
import { CgFrmRegistroContable } from './cg_frm_registro_contable';
import { CgFrmClientesProveedores } from './cg_frm_clientes_proveedores';
import { CgFrmLibroDiario } from './cg_frm_libro_diario';
import { CgFrmLibroMayor } from './cg_frm_libro_mayor';
/*
import { Form1 } from './form1';
import { Form2 } from './form2';
import { Form3 } from './form3';

//export * from './form1';
//export * from './form2';
*/
export const ContabilidadFormsRoute = { 
    show
};

function show(idForm,authentication){
    let form = "";
    console.log("authentication",authentication);
    
    switch(idForm){
        case 0:
            form = <CgFrmRegistroContable authentication={authentication} />            
            break;
        case 1:
            form = <CgFrmClientesProveedores authentication={authentication} />
            break;
        case 2:
            form = <CgFrmLibroDiario authentication={authentication} />
            break;
        case 3:
            form = <CgFrmLibroMayor authentication={authentication} />
            break;        
    }

    return (form);
}
