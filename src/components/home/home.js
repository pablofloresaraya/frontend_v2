import React, {Component} from 'react';
import { connect } from 'react-redux';
import { menuActions } from '../_actions';

import {HomeView} from './homeview'

class Home extends Component{
    constructor(props){
      super(props);
      this._handleOnClickModule = this._handleOnClickModule.bind(this);
    }

    _handleOnClickModule(p_id,p_name) {
        this.props.showModule(p_id, p_name);
    }

    render(){
      console.log("1. Home", this.props);

      return (
        <HomeView
          menuModuleActive={this.props.menuModuleActive}
          menuNameModuleActive={this.props.menuNameModuleActive}
          menuShowModule={this.props.showModule}
          _handleOnClickModule={this._handleOnClickModule}
        />
      );
    }
}

function mapStateToProps(state) {
    const menu = state.menu;
    return menu;
}

const mapDispatchToProps = {
  showModule: menuActions.showModule
};

const connectHome = connect(mapStateToProps, mapDispatchToProps)(Home);
export { connectHome as Home };