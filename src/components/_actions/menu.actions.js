import { menuConstants } from '../_constants';
import { menuService } from '../_services';
import { alertActions } from './';

export const menuActions = {
    showModule,
    showSectionsModule,
    setModuleUiMenu,
};

function showModule(p_moduleId, p_moduleName) {
    return dispatch => {
        dispatch(request(p_moduleId, p_moduleName));

        menuService.getModuleUiMenu(p_moduleId)
        .then(
            data => {
                var sections = [];
                var v_module_id = p_moduleId
                data.forEach(element => {
                    if(parseInt(element.parent_id)===parseInt(v_module_id))
                        sections.push(element);
                })

                dispatch(success(sections,data));
            },
            error => {
                //dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );
    };

    function request(p_moduleId, p_moduleName) { return { type: menuConstants.SHOW_MODULE, moduleId: p_moduleId, moduleName: p_moduleName } }
    function success(p_sections, p_uimenu) { return { type: menuConstants.SET_UI_MENU_SUCCESS, uimenu: p_uimenu, sections: p_sections } }
}

function showSectionsModule(p_sections) {
    return dispatch => {
        dispatch(success(p_sections));
    };

    function success(p_sections) { return { type: menuConstants.SHOW_SECTIONS_SUCCESS, sections: p_sections } }
}

function setModuleUiMenu(p_uimenu) {
    return dispatch => {
        dispatch(success(p_uimenu));
    };

    function success(p_uimenu) { return { type: menuConstants.SET_UI_MENU_SUCCESS, uimenu: p_uimenu } }
}