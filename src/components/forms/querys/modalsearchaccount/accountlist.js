import React, {Component} from 'react'; 
import { config } from '../../../_constants';
import { authHeader } from '../../../_helpers';

export class AccountList extends Component{

    constructor(props){
        super(props);
        
        this.state = {rows: {}, Data: [], filtro1: '', timeout: null};
        this._handleClick = this._handleClick.bind(this);
        this._handleOnKeyUp = this._handleOnKeyUp.bind(this);
        this.getData = this.getData.bind(this);
    }
    
    getData(){
        console.log("getData");

        const requestOptions = {
            method: 'GET',
            headers: authHeader()
        };
        
        fetch(`${config.apiUrl}/accounts?var1=1&var2=`, requestOptions)
            .then(results => {
                return results.json();
            })
            .then(results => {
                let data = results.data;
                if(data!==undefined){
                    this.setState({Data: data});
                }
            })
    }

    _handleClick(e,row){
        this.props.setValue(row);
        this.props.close();
    }

    _handleOnKeyUp = (event) => {
        console.log("_handleOnKeyUp")
        clearTimeout(this.state.timeout)
        this.setState({
            timeout: setTimeout(() => {
                this.getData();
                clearTimeout(this.state.timeout);
            },800)
        })      
    }

    _handleOnChangeFiltro1(event){
        this.setState({filtro1: event.target.value})
    }

    componentDidMount(prevProps) {
        console.log("componentDidUpdate")
        // Uso tipico (no olvides de comparar los props):
        //if (this.props.userID !== prevProps.userID) {
          this.getData();
        //}
      }

    render(){

        return(
            <div>
                <form className="ml-3"> 
                    <div className="form-group row">
                        <label htmlFor="filtro1" className="col-sm-2 col-form-label">Nombre</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control form-control-xs" id="filtro1" 
                                value={this.state.filtro1}
                                onChange={this._handleOnChangeFiltro1.bind(this)}
                                onKeyUp={this._handleOnKeyUp} />
                        </div>
                    </div>
                </form>
                <div className="table-responsive">
                    <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                        <thead>
                            <tr>
                                <th style={{width: '40px', }} >Cuenta</th>
                                <th>Nombre</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.Data.map((row, index) => {
                                return(  
                                    <tr key={row.id} onClick={(e) => this._handleClick(e, row)} style={{cursor: 'pointer', }}>
                                        <td>{row.account_code}</td>
                                        <td>{row.name}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
};

export default AccountList;