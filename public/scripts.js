    /*
    * Start Bootstrap - SB Admin v6.0.0 (https://startbootstrap.com/templates/sb-admin)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-sb-admin/blob/master/LICENSE)
    */
(function() {
    "use strict";

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
    var layout = document.getElementById("layoutSidenav_nav");
    
    var nav_link = layout.querySelectorAll(".sb-sidenav a.nav-link");
    for(var i = 0; i < nav_link.length; i++){
        if (nav_link[i].href === path) {
            nav_link[i].className += " active";
        }
    };
})();
