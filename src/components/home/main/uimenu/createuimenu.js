import React, {Component} from 'react';

import {NavbarNav} from "./navbarnav";
import {NavItem} from "./navitem";
import {NavLink} from "./navlink";
import {DropdownMenu} from "./dropdownmenu";
import {DropdownSubMenu} from "./dropdownsubmenu";
import {DropdownItem} from "./dropdownitem";

//export function ViewUIMenu(props){
export class ViewUIMenu extends Component{
    constructor(props) {
        super(props);
        this.state = {uimenu: []};

        this._showUIMenu = this._showUIMenu.bind(this);
    }

    _showUIMenu(self,p_uimenuhor,p_uimenu,p_moduleId,p_iteracion){
        
        //const p_uimenuhor = this.props.uimenuhor;
        //const p_uimenu = this.props.uimenu;
        //const p_moduleId = this.props.moduleId;
        //const p_iteracion = this.props.iteracion;
        
        if (p_iteracion==0){

            if(p_uimenu.length==0){
                return(<NavbarNav></NavbarNav>);
            }
            else{
                var uimenuhor = [];
                p_uimenu.forEach(function(p_menu){
                    if(p_menu.parent_id==p_moduleId)
                        uimenuhor.push(p_menu);
                })
                
                //return(
                //    <NavbarNav>
                //        <ViewUIMenu
                //            uimenuhor={uimenuhor}
                //            uimenu={p_uimenu}
                //            moduleId={p_moduleId}
                //            iteracion={p_iteracion+1}
                //        />
                //    </NavbarNav>
                //);
                let datos = self._showUIMenu(self,uimenuhor,p_uimenu,p_moduleId,(p_iteracion+1));
                return(
                    <NavbarNav>
                        {datos}
                    </NavbarNav>
                )
            }
        }
        else if(p_iteracion==1){

            p_uimenuhor.forEach(
                function(p_menu){
                    var v_hijos = [];
                    p_uimenu.forEach(function(p_hijo){
                        if(parseInt(p_hijo.parent_id)==parseInt(p_menu.id))
                            v_hijos.push(p_hijo);
                    });

                    console.log("NavItem: ", p_menu);
                    console.log("NavItem Hijo: ", v_hijos);
                    //return(
                    //    <NavItem>
                    //        <NavLink name={p_menu.name} />
                    //        <DropdownMenu>
                    //            <ViewUIMenu
                    //                uimenuhor={[]}
                    //                uimenu={v_hijos}
                    //                moduleId={p_moduleId}
                    //                iteracion={p_iteracion+1}
                    //            />
                    //        </DropdownMenu>
                    //    </NavItem>
                    //)
                    let datos = self._showUIMenu(self,p_uimenuhor,v_hijos,p_moduleId,(p_iteracion+1));
                    return(
                        <NavItem>
                            <NavLink name={p_menu.name} />
                            <DropdownMenu>
                                {datos}
                            </DropdownMenu>
                        </NavItem>
                    )
                }
            )
        }
        else{
            p_uimenu.forEach(
                function(p_menu){
                    console.log('p_menu: ', p_menu);

                    if(p_menu.menu_type=="menu"){
                        var v_hijos = [];
                        p_uimenu.forEach(function(p_hijo){
                            if(p_hijo.parent_id==p_menu.id)
                                v_hijos.push(p_hijo);
                        });
                        console.log("DropdownSubMenu: ", p_menu);
                        //return(
                        //    <DropdownSubMenu name={p_menu.name}>
                        //        <DropdownMenu>
                        //            <ViewUIMenu
                        //                uimenuhor={[]}
                        //                uimenu={v_hijos}
                        //                moduleId={p_moduleId}
                        //                iteracion={p_iteracion+1}
                        //            />
                        //        </DropdownMenu>
                        //    </DropdownSubMenu>
                        //)
                        let datos = self._showUIMenu(self,p_uimenuhor,v_hijos,p_moduleId,(p_iteracion+1));
                        return(
                            <DropdownSubMenu name={p_menu.name}>
                                <DropdownMenu>
                                    {datos}
                                </DropdownMenu>
                            </DropdownSubMenu>
                        )
                    }
                    else{
                        console.log("DropdownItem: ", p_menu);
                        return(
                            <DropdownItem name={p_menu.name} />
                        )
                    }
                }
            )
        }
        console.log("Default DropdownItem: ", p_uimenu, p_moduleId, p_iteracion);
        return(
            <DropdownItem></DropdownItem>
        )
    }

    render(){
        //let elements = null;
        //const { children } = this.props
        //
        //if(this.props.uimenu){
        //    elements = this.props.uimenu.map((element) => {
        //        return(
//
        //        )
        //    })
        //}
        console.log("4. ViewUIMenu props", this.props);
        let datos = this._showUIMenu(this,this.props.uimenuhor,this.props.uimenu,this.props.moduleId,0);
        console.log('------------------------------------------------');
        console.log('datos: ', datos);
        console.log('------------------------------------------------');

        return(
            <NavbarNav>
                {elements}
            </NavbarNav>
        );
    }
}