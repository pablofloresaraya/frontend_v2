import React, {Component} from 'react'; 
import { config } from '../../../_constants';
import { authHeader, base64EncodeUnicode } from '../../../_helpers';

export class ProvincesTypeList extends Component{

    constructor(props){
        super(props);
        
        this.state = {rows: {}, Data: [], filtro1: '', timeout: null};
        this._handleClick = this._handleClick.bind(this);
        this._handleOnKeyUp = this._handleOnKeyUp.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData(){

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("territorial");
        let v_option = base64EncodeUnicode("2");
        
        const requestOptions = { 
            method: 'POST',
        body: JSON.stringify({ region: this.props.region })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const resp = await response.json();                      
            this.setState({Data:resp.data});
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    _handleClick(e,row){
        this.props.setValue(row);
        this.props.close();
    }

    _handleOnKeyUp = (event) => {
        console.log("_handleOnKeyUp")
        clearTimeout(this.state.timeout)
        this.setState({
            timeout: setTimeout(() => {
                this.getData();
                clearTimeout(this.state.timeout);
            },800)
        })      
    }

    _handleOnChangeFiltro1(event){
        this.setState({filtro1: event.target.value})
    }

    componentDidMount(prevProps) {
        console.log("componentDidUpdate")
        // Uso tipico (no olvides de comparar los props):
        //if (this.props.userID !== prevProps.userID) {
          this.getData();
        //}
    }

    render(){

        return(
            <div>
                <div className="table-responsive">
                    <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                        <thead>
                            <tr>
                                <th style={{width: '40px', }} >#</th>
                                <th>Nombre</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.Data.map((row, index) => {
                                return(  
                                    <tr key={row.id} onClick={(e) => this._handleClick(e, row)} style={{cursor: 'pointer', }}>
                                        <td>{index+1}</td>
                                        <td>{row.name}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
};

export default ProvincesTypeList;