import React, {Component} from 'react';
import $ from 'jquery';
import { config } from '../../_constants';
import {authHeader, base64EncodeUnicode} from '../../_helpers';
import {ButtonRutProv} from '../querys/modalrutprov/ButtonRutProv';
import {ButtonRegions} from '../querys/modalRegions/ButtonRegions';
import {ButtonProvinces} from '../querys/modalProvinces/ButtonProvinces';
import {ButtonCommunes} from '../querys/modalCommunes/ButtonCommunes';
import Modal from 'react-bootstrap/Modal';
import {MenuForm} from '../menuform/menuform';
import {  validate, clean, format, getCheckDigit } from 'rut.js';

export class CgFrmClientesProveedores extends Component{

    constructor(props){ 

        super(props);

        let v_cuenta = 1;
        let v_usuario = 1;

        this.state = {
            form: { 
                     id: ""
                    ,rut: ""
                    ,dig:""
                    ,cliente: false
                    ,proveedor: false
                    ,nombre: ""
                    ,giro: ""
                    ,direccion: ""
                    ,region: {id:"",name:""}
                    ,provincia: {id:"",name:""}
                    ,comuna: {id:"",name:""}
                    ,telefono: ""
                    ,celular: ""
                    ,email: ""
                    ,sucursal: ""
                    ,email_suc: ""
                    ,btn:true
                    ,cuenta: v_cuenta
                    ,usuario: v_usuario                     
                },
            validate: { 
                        email: { status:false, msj:"", clase:"" }
                        ,emailSuc: { status:false, msj:"", clase:"" }
                    },
            regions:[],
            provinces:[],
            communes:[],
            ruts:[],
            accounts:[],            
            provincesxregions:[],
            communesxprovinces:[],            
            modal : {msg:"", clase:""},
            confirm : {show:false, msg:"", clase:""}
                         
        } 

        this.handleChange        = this.handleChange.bind(this);
        this.handleSendData      = this.handleSendData.bind(this);       
        this.handleDeleteData    = this.handleDeleteData.bind(this);
        this.handleChangeNum     = this.handleChangeNum.bind(this);
        this.handleChangeRut     = this.handleChangeRut.bind(this);
        this.handleClickCheck    = this.handleClickCheck.bind(this);       
        this.handleSetRut        = this.handleSetRut.bind(this);
        this.handleSetProvinces  = this.handleSetProvinces.bind(this);
        this.handleSetCommunes   = this.handleSetCommunes.bind(this);
        this.handleSetRegion     = this.handleSetRegion.bind(this);
        this.validateEmail       = this.validateEmail.bind(this);
        this.handleClickClose    = this.handleClickClose.bind(this);
        this.handleClickShow     = this.handleClickShow.bind(this);
        this.handleShowMOdal     = this.handleShowModal.bind(this);
        this.handleCleanForm     = this.handleCleanForm.bind(this);
        this.handleCloseTab      = this.handleCloseTab.bind(this);
        
    }

    /*getData(){

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("territorial");
        let v_option = base64EncodeUnicode("1");
        
        const requestOptions = { 
        method: 'GET',
        //headers: authHeader(),
        //body: formData //JSON.stringify({ title: 'React POST Request Example' })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const data = await response.json();
            this.setState(data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });

    }*/

    componentDidMount(prevProps) {
        //console.log("componentDidUpdate-> "+this.state.btn);
        // Uso tipico (no olvides de comparar los props):
        //if (this.props.userID !== prevProps.userID) {
          //this.getData();
        //}
    }

    handleChange(event){

        const { name, value } = event.target;

        this.setState({
            form: {
                ...this.state.form,
                [name] : value
            }             
        })    
        //this.props.datosProv(name,value);
    }

    handleChangeNum(event){

        const { name, value } = event.target;
        
        const removeNonNumeric = num => num.toString().replace(/[^0-9]/g, "");

        this.setState({
            form: {
                ...this.state.form,
                [name] : removeNonNumeric(value)
            }             
        })    
        
    }

    handleChangeRut(event){

        const { name, value } = event.target;
        
        const removeNonNumeric = num => num.toString().replace(/[^0-9]/g, "");

        this.setState({
            form: {
                ...this.state.form,
                [name] : removeNonNumeric(value),
                dig: value ? getCheckDigit(removeNonNumeric(value)) : ""
            }             
        })    
        
    }

    handleClickCheck(event){

        const { name } = event.target;

        let check;

        check = (!this.state.form[name]) ? true : !this.state.form[name];
        
        this.setState({
          form: {
            ...this.state.form,
            [name] : check
          }           
        })       
               
    }

    validateEmail(e,tipo) {
        
        const { name, value } = e.target;        

        const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;
        const { validate } = this.state;

        this.setState({
            form: {
                ...this.state.form,
                [name] : value
            }             
        })

        if(tipo==="EM"){ //email

            if (emailRex.test(value)) {
                validate.email.status = true;
                validate.email.clase  = "text-success";
                validate.email.msj    = "Email Correcto";
            } else {
                validate.email.status = true;
                validate.email.clase  = "text-danger";
                validate.email.msj    = "Email Incorrecto";
            }

            if(value===""){
                validate.email.status = false;
                validate.email.clase  = "";
                validate.email.msj    = "";
            }

        }else{ //email sucursal

            if (emailRex.test(value)) {
                validate.emailSuc.status = true;
                validate.emailSuc.clase  = "text-success";
                validate.emailSuc.msj    = "Email Correcto";
            } else {
                validate.emailSuc.status = true;
                validate.emailSuc.clase  = "text-danger";
                validate.emailSuc.msj    = "Email Incorrecto";
            }

            if(value===""){
                validate.emailSuc.status = false;
                validate.emailSuc.clase  = "";
                validate.emailSuc.msj    = "";
            }

        }

        this.setState({ validate });

    }

    handleSetRut(p_run) {
    
        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("partners");
        let v_option = base64EncodeUnicode("4");
        
        const requestOptions = { 
            method: 'POST',
            body:  JSON.stringify({ run : p_run })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const data = await response.json();                                
            this.setState({form:data});
            //this.forceUpdate();
        })
        .catch((error) => {
            console.error('Error:', error);
        });
              
    }

    handleSetRegion(p_row) {
        
        //limpio provincia y comuna
        this.setState(prevState => ({
            form: {
                ...prevState.form,           
                provincia: {                     
                    ...prevState.form.provincia,   
                    id : "",
                    name : ""          
                },
                comuna: {                     
                    ...prevState.form.comuna,   
                    id : "",
                    name : ""          
                }
            }
        }));

        //add region
        this.setState(prevState => ({
            form: {
                ...prevState.form,           
                region: {                     
                    ...prevState.form.region,   
                    id : p_row.id,
                    name : p_row.name          
                }
            }
        }))

    }

    handleSetProvinces(p_row) {

        //limpio comuna
        this.setState(prevState => ({
            form: {
                ...prevState.form,           
                comuna: {                     
                    ...prevState.form.comuna,   
                    id : "",
                    name : ""          
                }
            }
        }));

        this.setState(prevState => ({
            form: {
                ...prevState.form,           
                provincia: {                     
                    ...prevState.form.provincia,   
                    id : p_row.id,
                    name : p_row.name          
                }
            }
        }));

        //filtra comunas x provincias
        //const communes = this.props.data.communes.filter(communes => communes.parent_id === p_id);
        //this.setState({communesxprovinces:communes});

    }

    handleSetCommunes(p_row) {

        this.setState(prevState => ({
            form: {
                ...prevState.form,           
                comuna: {                     
                    ...prevState.form.comuna,   
                    id : p_row.id,
                    name : p_row.name          
                }
            }
        }))

    }
    
    validateEmailSend = (email) => {
        const re =
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;
        return re.test(String(email).toLowerCase());
    }

    handleClickShow(){ //modal confirmar

        if(this.state.form.id===""){
            this.setState({
                modal: {
                    ...this.state.modal,                            
                        msg   : "Debe seleccionar desde la lista.",
                        clase : "modal-header-danger"                               
                }             
            })
            this.handleShowModal();
            return false;
        }

        $(document.getElementById("accConfirm")).modal("show");
    }

    handleClickClose(event){ //modal confirmar
        $(document.getElementById("accConfirm")).modal("hide");  
    }

    handleShowModal(){ //modal alert
        $(document.getElementById("accAlerta")).modal("show");
    }

    handleSendData() { //insert-update

        if(!this.state.form.cliente && !this.state.form.proveedor){
            this.setState({
                modal: {
                    ...this.state.modal,                        
                        msg   : "Seleccione cliente o proveedor",
                        clase : "modal-header-danger"                         
                }             
            })
            this.handleShowModal();
            return false;
        }

        const obligatorios = ['rut','nombre','direccion','region','provincia','comuna'];

        let conta=0;
        let msg="";

        outer_loop: for (const [key, value] of Object.entries(this.state.form)){                       
            for(let i=0;i<obligatorios.length;i++){
                if(key===obligatorios[i]){ 
                    if(Object.keys(value).length===0){
                        if(value===""){                        
                            msg=obligatorios[i];
                            conta++;
                            break outer_loop;
                        }                       
                    }else{
                        if(value.name===""){                        
                            msg=obligatorios[i];
                            conta++
                            break outer_loop;
                        } 
                    }                     
                }
            }            
        }

        if(conta>0){
            this.setState({
                modal: {
                    ...this.state.modal,                        
                        msg   : "Ingrese "+msg,
                        clase : "modal-header-danger"                         
                }             
            })
            this.handleShowModal();
            return false;
        }
          
        if(this.state.form.email!==""){
            if(!this.validateEmailSend(this.state.form.email)){
                this.setState({
                    modal: {
                        ...this.state.modal,                            
                            msg   : "Existe email incorrecto.",
                            clase : "modal-header-danger"                               
                    }             
                })
                this.handleShowModal();
                return false; 
            }
        }    

        if(this.state.form.emailSuc!==""){
            if(!this.validateEmailSend(this.state.form.emailSuc)){
                this.setState({
                    modal: {
                        ...this.state.modal,                            
                            msg   : "Existe email incorrecto.",
                            clase : "modal-header-danger"                               
                    }             
                })
                this.handleShowModal();
                return false; 
            }
        }    

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("partners");
        let v_option = base64EncodeUnicode("5");
        
        const requestOptions = { 
            method: 'POST',
            body:  JSON.stringify( this.state.form )
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const data = await response.json();

            if(data.status){
                this.handleSetRut(this.state.form.rut);                                             
            }else{
                this.setState({
                    modal: {
                        ...this.state.modal,
                        msg : data.message,
                        clase: data.clase
                    }             
                }) 
                this.handleShowModal();
            }
        })
        .catch((error) => {
            console.error('Error:', error);
        });

    }

    handleDeleteData() {

        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("6");
        
        const requestOptions = { 
            method: 'POST',
            body:  JSON.stringify({ 
                                    id:this.state.form.id, 
                                    cliente:this.state.form.cliente,
                                    proveedor:this.state.form.proveedor
                                })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
        .then(async response => {
            const data = await response.json();
               
            if(data.status){
                this.handleSetRut(this.state.form.rut);
                this.handleCleanForm();
                this.handleClickClose(); //close modal confirm
            }else{
                alert('error de sistema')
            }        
            
        })
        .catch((error) => {
            console.error('Error:', error);
        });

    }

    handleCleanForm() {

        this.setState({
            form: {
                ...this.state.form, 
                id: ""
                ,rut: ""
                ,dig:""
                ,cliente: false
                ,proveedor: false
                ,nombre: ""
                ,giro: ""
                ,direccion: ""
                ,region: {id:"",name:""}
                ,provincia: {id:"",name:""}
                ,comuna: {id:"",name:""}
                ,telefono: ""
                ,celular: ""
                ,email: ""
                ,sucursal: ""
                ,email_suc: ""
                ,btn: true
            }
        });

    }

    handleCloseTab() {
        //this.props.authentication={};
    }
   
    render(){
        
        const menuitems = [ 
            {type: 'GUARDAR', onClick: this.handleSendData},
            {type: 'ELIMINAR', onClick: this.handleClickShow, disabled: this.state.form.btn},
            {type: 'NUEVO', onClick: this.handleCleanForm},
            {type: 'CERRAR', onClick: this.handleCloseTab}
        ];

        return( 
        <>           
            <div className="row" >
                <div className="col-lg-12 col-md-12">                    
                    { !this.state.form.btn && <MenuForm menuitems={menuitems} /> }
                    { this.state.form.btn && <MenuForm menuitems={menuitems} /> }                    
                    <div className="card">
                        <div className="card-header">
                            <strong>Informaci&oacute;n del Proveedor</strong>
                        </div>
                        <div className="card-body">
                            <form name="clieProvForm" className="form-horizontal" style={{marginTop:"1rem"}}>                      
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Rut</label>
                                    <div className="col-md-2">                                     
                                        <input name="rut" type="text" className="form-control form-control-xs text-center" maxLength="8" value={this.state.form.rut} onChange={this.handleChangeRut} placeholder="RUT" />
                                    </div> 
                                    <div className="col-md-1">
                                        <div className="input-group input-group-xs">                                                                                                                   
                                            <input name="dig" type="text" className="form-control form-control-xs text-center" style={{maxWidth:"40px"}} value={this.state.form.dig} disabled />                                       
                                            <div className="input-group-append">
                                                <ButtonRutProv
                                                    modalId="cnRutType"
                                                    setValue={this.handleSetRut}
                                                />
                                            </div>
                                        </div>                                              
                                    </div>
                                    <div className="col-md-2 col-lg-1">
                                        <div className="input-group input-group-xs form-check form-check-inline">
                                            <input className="form-check-input" type="checkbox" name="cliente" id="cliente" value={this.state.form.cliente} checked={this.state.form.cliente} onChange={this.handleClickCheck} />
                                            <label className="form-check-label col-form-label-xs control-label input-label" htmlFor="cliente">
                                                Cliente
                                            </label>
                                        </div>
                                    </div>                            
                                    <div className="col-md-2 col-lg-2">
                                        <div className="input-group input-group-xs form-check form-check-inline">
                                            <input className="form-check-input" type="checkbox" name="proveedor" id="proveedor" value={this.state.form.proveedor} checked={this.state.form.proveedor} onChange={this.handleClickCheck} />
                                            <label className="form-check-label col-form-label-xs control-label input-label" htmlFor="proveedor">
                                                Proveedor
                                            </label>
                                        </div>
                                    </div>                            
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Nombre</label>
                                    <div className="col-md-5">
                                        <input name="nombre" type="text" className="form-control form-control-xs input-sm text-left" value={this.state.form.nombre} onChange={this.handleChange} placeholder="Nombre" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Giro</label>
                                    <div className="col-md-5">
                                        <input name="giro" type="text" className="form-control form-control-xs input-sm text-left" value={this.state.form.giro} onChange={this.handleChange} placeholder="Giro" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Direcci&oacute;n</label>
                                    <div className="col-md-5">
                                        <input name="direccion" type="text" className="form-control form-control-xs input-sm text-left" value={this.state.form.direccion} onChange={this.handleChange} placeholder="Direccion" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Regi&oacute;n</label>
                                    <div className="col-md-5">
                                        <div className="input-group input-group-xs">
                                            <input type="text" className="form-control form-control-xs" value={this.state.form.region.name} placeholder="Regi&oacute;n" aria-label="comuna" aria-describedby="button-comuna" disabled />
                                            <div className="input-group-append">
                                                <ButtonRegions 
                                                    modalId="cnRegionType"
                                                    setValue={this.handleSetRegion}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Provincia</label>
                                    <div className="col-md-5">
                                        <div className="input-group input-group-xs">
                                            <input type="text" className="form-control" value={this.state.form.provincia.name} placeholder="Provincia" aria-label="ciudad" aria-describedby="button-ciudad" disabled />
                                            <div className="input-group-append">
                                                <ButtonProvinces 
                                                    modalId="cnProvinceType"
                                                    setValue={this.handleSetProvinces}
                                                    region={this.state.form.region.id} 
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Comuna</label>
                                    <div className="col-md-5">
                                        <div className="input-group input-group-xs">
                                            <input type="text" className="form-control" value={this.state.form.comuna.name} placeholder="Comuna" aria-label="ciudad" aria-describedby="button-ciudad" disabled />
                                            <div className="input-group-append">
                                                <ButtonCommunes 
                                                    modalId="cnCommuneType"
                                                    setValue={this.handleSetCommunes}
                                                    provincia={this.state.form.provincia.id}   
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Telefono</label>
                                    <div className="col-md-2">
                                        <div className="input-group input-group-xs">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text" id="btnGroupAddon">+57</div>
                                            </div>
                                            <input name="telefono" type="text" className="form-control input-sm text-left" maxLength="7" value={this.state.form.telefono} onChange={this.handleChangeNum} placeholder="Telefono" />
                                        </div>
                                    </div>
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Celular</label>
                                    <div className="col-md-2">
                                        <div className="input-group input-group-xs">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text" id="btnGroupAddon">+56</div>
                                            </div>
                                            <input name="celular" type="text" className="form-control input-sm text-left" maxLength="9" value={this.state.form.celular} onChange={this.handleChangeNum} placeholder="Celular" />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Email</label>
                                    <div className="col-md-5">
                                        <input name="email" type="text" className="form-control form-control-xs input-sm text-left" value={this.state.form.email} onChange={(e) => this.validateEmail(e,"EM")} placeholder="Email" />
                                    </div>
                                    {this.state.validate.email.status && <label className={`col-md-2 col-lg-2 col-form-label-xs control-label input-label text-left ${this.state.validate.email.clase}`}>{ this.state.validate.email.msj }</label> }                                 
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Sucursal</label>                                
                                    <div className="col-md-5">
                                        <input name="sucursal" type="text" className="form-control form-control-xs input-sm text-left" value={this.state.form.sucursal} onChange={this.handleChange} placeholder="Sucursal" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-2 col-lg-1 col-form-label-xs control-label input-label text-right">Email Suc.</label>
                                    <div className="col-md-5">
                                        <input name="email_suc" type="text" className="form-control form-control-xs input-sm text-left" value={this.state.form.email_suc} onChange={(e) => this.validateEmail(e,"ES")} placeholder="Email Sucursal" />
                                    </div>
                                    {this.state.validate.emailSuc.status && <label className={`col-md-2 col-lg-2 col-form-label-xs control-label input-label text-left ${this.state.validate.emailSuc.clase}`}>{ this.state.validate.emailSuc.msj }</label> } 
                                </div>                      
                            </form>
                        </div>
                    </div>   
                </div>
            </div>

            <div className="modal fade" tabIndex="-1" role="dialog" id={"accAlerta"}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className={`modal-header ${this.state.modal.clase}`}>
                            <h5 className="modal-title"><i className="fas fa-exclamation-triangle"></i></h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">                                       
                            <span>{this.state.modal.msg}</span>
                        </div>
                        <div className="modal-footer">                            
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.handleClose}>Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade" tabIndex="-1" role="dialog" id={"accConfirm"}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                    <div className="modal-header modal-header-primary">
                        <h5 className="modal-title">Confirmar</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">                                       
                        <span>Est&aacute; seguro de eliminar el registro?</span>
                    </div>
                    <div className="modal-footer">                            
                        <button type="button" className="btn btn-primary" onClick={this.handleDeleteData}>Aceptar</button>                         
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                    </div>
                </div>
            </div>
        </>          
        )
    }

}

