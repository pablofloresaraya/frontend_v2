import React from 'react';

export function Module(props){ 
    console.log('props.moduleActive: '+props.moduleActive);
    return(
        <a className={(props.moduleActive===props.moduleId) ? 'nav-link active' : 'nav-link'}  href={props.moduleUrl} 
            onClick={(e) => {props.onClickModule(props.moduleId)}} > 
            <div className="sb-nav-link-icon">
                <i className="fas fa-tachometer-alt"></i>
            </div>
            {props.moduleName}
        </a>
    );
    
}

export default Module;