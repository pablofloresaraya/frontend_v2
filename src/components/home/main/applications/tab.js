import React, {Component} from 'react';

export class Tab extends Component{
    constructor(props){
      super(props);
      this._handleClick = this._handleClick.bind(this);
    }

    _handleClick(){
      this.props.onClick(this.props.id);
    }

    render(){
      return(
        <>
            <li className="nav-item">
                <a  className={(this.props.applicationActive===this.props.id) ? 'nav-link active' : 'nav-link'}
                    id={this.props.id+"-tab"} 
                    data-toggle="tab" 
                    href={"#"+this.props.id} 
                    role="tab" 
                    aria-controls={this.props.id} 
                    aria-selected="true"
                    onClick={this._handleClick}
                  >
                    {this.props.name}
                </a>
            </li>
        </>
      );
    }
  }