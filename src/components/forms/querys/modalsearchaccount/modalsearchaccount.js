import React from 'react';

//import {AccountList} from './accountlist'; 
import {TreeView} from '../../../_librerias/treeview/src/treeview/TreeView'
//import dataJson from '../../../_librerias/treeview/src/assets/data/accounts.json'
import { config } from '../../../_constants';
import { authHeader, base64EncodeUnicode } from '../../../_helpers';

class ModalSearchAccount extends React.Component{

    constructor(props){ 
        super(props);
        
        this.state = {Data: [], filtro1: '', timeout: null};
        this._handleClick = this._handleClick.bind(this);
        this._handleOnKeyUp = this._handleOnKeyUp.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData(){
        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        var v_company = base64EncodeUnicode(company.id);
        var v_route = base64EncodeUnicode("accounts");
        var v_option = base64EncodeUnicode("1");
        
        const formData = new FormData();
        formData.append( "var1", "");

        const requestOptions = {
            method: 'POST',
            //headers: authHeader()
            body: formData
        };
        
        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(results => {
                return results.json();
            })
            .then(results => {
                let data = results.data;
                if(data!==undefined){
                    this.setState({Data: data});
                }
            })
    }
    /*
    getData(){
        console.log("getData");

        const requestOptions = {
            method: 'GET',
            headers: authHeader()
        };
        
        //fetch('http://nefo:8082/vouchertypes?limit=10&var1=1', requestOptions)
        fetch(`${config.apiUrl}/accounts?var1=1&var2=`, requestOptions)
            .then(results => {
                return results.json();
            })
            .then(results => {
                let data = results.data;
                if(data!==undefined){
                    this.setState({Data: data});
                }
            })
    }
    */
    _handleClick(e){
        let api = this.refs.treeview.api;
        let selectedItem = api.getSelectedItem();
        console.log("selectedItem",selectedItem)
        this.props.setValue(selectedItem);
        this.props.close();
    }

    _handleOnKeyUp = (event) => {
        console.log("_handleOnKeyUp")
        clearTimeout(this.state.timeout)
        this.setState({
            timeout: setTimeout(() => {
                this.getData();
                clearTimeout(this.state.timeout);
            },800)
        })      
    }

    _handleOnChangeFiltro1(event){
        this.setState({filtro1: event.target.value})
    }

    componentDidMount(prevProps) {
        console.log("componentDidUpdate")
        // Uso tipico (no olvides de comparar los props):
        //if (this.props.userID !== prevProps.userID) {
          this.getData();
        //}
      }

    addItemAndSubItem() {
        let api = this.refs.treeview.api;
    
        let selectedItem = api.getSelectedItem();
    
        if (selectedItem) {
          api.removeItem(selectedItem.id);
        } else {
          alert('You have to select a item to remove it');
        }
      }

    render(){
        
        return(
            <div className="modal fade" tabIndex="-1" role="dialog" id={this.props.id}>
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Cuentas</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <button onClick={ this.addItemAndSubItem.bind(this) }>Click to remove the select item</button>
                        <TreeView ref="treeview" items={this.state.Data} />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={this._handleClick}>Aceptar</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.props.close}>Cerrar</button>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
    
}

export default ModalSearchAccount;