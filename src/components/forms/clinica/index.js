import React, {Component} from 'react';

import { FormInicio } from './forminicio';
import { Form1 } from './form1';
import { Form2 } from './form2';
import { Form3 } from './form3';

//export * from './form1';
//export * from './form2';
export const ClinicaFormsRoute = {
    show
};

function show(idForm){
    let form = "";
    switch(idForm){
        case 0:
            form = FormInicio.show();
            break;
        case 21:
            form = Form1.show()
            break;
        case 37:
            form = <Form3 />
            break;
    }
    return (form);
}