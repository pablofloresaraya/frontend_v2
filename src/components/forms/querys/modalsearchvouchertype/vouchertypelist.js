import React, {Component} from 'react'; 
import { config } from '../../../_constants';
import { authHeader, base64EncodeUnicode } from '../../../_helpers';

export class VoucherTypeList extends Component{

    constructor(props){
        super(props);
        
        this.state = {rows: {}, Data: [], filtro1: '', timeout: null};
        this._handleClick = this._handleClick.bind(this);
        this._handleOnKeyUp = this._handleOnKeyUp.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData(){
        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        var v_company = base64EncodeUnicode(company.id);
        var v_route = base64EncodeUnicode("vouchertypes");
        var v_option = base64EncodeUnicode("1");
        
        const requestOptions = {
            method: 'POST',
            //headers: authHeader()
        };
        
        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(results => {
                return results.json();
            })
            .then(results => {
                let data = results.data;
                if(data!==undefined){
                    this.setState({Data: data});
                }
            })
    }
    /*
    getData(){
        console.log("getData");

        const requestOptions = {
            method: 'GET',
            headers: authHeader()
        };
        
        //fetch('http://nefo:8082/vouchertypes?limit=10&var1=1', requestOptions)
        fetch(`${config.apiUrl}/vouchertypes?limit=10&var1=1`, requestOptions)
            .then(results => {
                return results.json();
            })
            .then(results => {
                let data = results.data;
                if(data!==undefined){
                    this.setState({Data: data});
                }
            })
    }
    */
    _handleClick(e,row){
        this.props.setValue(row);
        this.props.close();
    }

    _handleOnKeyUp = (event) => {
        console.log("_handleOnKeyUp")
        clearTimeout(this.state.timeout)
        this.setState({
            timeout: setTimeout(() => {
                this.getData();
                clearTimeout(this.state.timeout);
            },800)
        })      
    }

    _handleOnChangeFiltro1(event){
        this.setState({filtro1: event.target.value})
    }

    componentDidMount(prevProps) {
        console.log("componentDidUpdate")
        // Uso tipico (no olvides de comparar los props):
        //if (this.props.userID !== prevProps.userID) {
          this.getData();
        //}
      }

    render(){

        return(
            <div>
                <form className="ml-3"> 
                    <div className="form-group row">
                        <label htmlFor="filtro1" className="col-sm-2 col-form-label">Nombre</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control form-control-xs" id="filtro1" 
                                value={this.state.filtro1}
                                onChange={this._handleOnChangeFiltro1.bind(this)}
                                onKeyUp={this._handleOnKeyUp} />
                        </div>
                    </div>
                </form>
                <div className="table-responsive">
                    <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                        <thead>
                            <tr>
                                <th style={{width: '40px', }} >C&oacute;digo</th>
                                <th>Nombre</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.Data.map((row, index) => {
                                return(  
                                    <tr key={row.id} onClick={(e) => this._handleClick(e, row)} style={{cursor: 'pointer', }}>
                                        <td>{row.voucher_type_code}</td>
                                        <td>{row.name}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
};

export default VoucherTypeList;