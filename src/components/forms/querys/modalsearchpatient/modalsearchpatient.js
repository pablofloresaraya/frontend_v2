import React, {useState} from 'react';

import {PatientList} from './PatientList'; 

export function ModalSearchPatient(props){

    const [pacientSelected,setPacientSelected] = useState({});
    
    return(
        <div className="modal" tabIndex="-1" role="dialog" id="_modalSearchPatient">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title">Pacientes</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    <PatientList  pacientSelected={pacientSelected}/> 
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={props.close}>Cerrar</button>
                </div>
                </div>
            </div>
        </div>
    );
    
}

export default ModalSearchPatient;