import React, {Component} from 'react';
import { menuService } from '../../../_services';

function Applicacion(props){
    function _handleOnClickApplication(){
        props.onClick({id: props.id, name: props.name});
    }
    return(
        <a className="dropdown-item" href="#" onClick={_handleOnClickApplication}>{props.name}</a>
    )
}
export class ModuleSection extends Component{
    constructor(props){
        super(props);
        this.state = {apps: []};
        
        this._handleOnClickApplication = this._handleOnClickApplication.bind(this);
    }

    _handleOnClickApplication(app){
        this.props.addApplicationTab(app);
    }

    componentDidMount(){

        menuService.getModuleApplications(this.props.moduleId,this.props.id)
        .then(
            result => {
                this.setState({apps: result.data});
            },
            error => {
                console.log("Menu error: "+error);
            }
        );
    }

    render(){

        return(
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.props.name}
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                {
                    this.state.apps.map((app) =>
                        <Applicacion 
                            id={app.md_cod}
                            name={app.md_nombre} 
                            onClick={this._handleOnClickApplication}

                        />
                    )
                }
                </div>
            </li>
        );
    }
}

export default ModuleSection;