import React, {Component} from 'react';

//import {NavbarNav} from "./navbarnav";
import {NavItem} from "./navitem";
import {NavLink} from "./navlink";
import {DropdownMenu} from "./dropdownmenu";
import {DropdownSubMenu} from "./dropdownsubmenu";
import {DropdownItem} from "./dropdownitem";

//export function ViewUIMenu(props){
export class ViewUIMenu extends Component{
    /*
    constructor(props) {
        super(props);
    }
    */
    render(){
        //const childrens = this.props.childrens;
        const p_uimenu = this.props.uimenu;
        const p_moduleId = this.props.moduleId;
        const p_iteracion = this.props.iteracion;
        const p_onclick = this.props._handleOnClickApplication;
        let elements = null;
        console.log("--------------------------------------");
        console.log("p_iteracion: ",p_iteracion);

       if(p_iteracion===1){
            console.log("this.props.childrens: ",this.props.childrens);
            elements = this.props.childrens.map(function(p_menu){
                    var v_hijos = [];
                    var p_hijo = [];
                    for(var i=0;i<p_uimenu.length;i++){
                        p_hijo = p_uimenu[i];
                        if(p_hijo.parent_id===p_menu.id)
                            v_hijos.push(p_hijo);
                    }

                    console.log("NavItem: ", p_menu);
                    console.log("NavItem Hijo: ", v_hijos);
                    return(
                        <NavItem key={p_menu.id}>
                            <NavLink name={p_menu.name} />
                            <DropdownMenu>
                                <ViewUIMenu
                                    childrens={v_hijos}
                                    uimenu={p_uimenu}
                                    moduleId={p_moduleId}
                                    iteracion={p_iteracion+1}
                                    _handleOnClickApplication={p_onclick}
                                />
                            </DropdownMenu>
                        </NavItem>
                    )
            })
            return(<>{elements}</>);
        }
        else if(p_iteracion>1){
            elements = this.props.childrens.map(p_menu => {
                    console.log('p_menu: ', p_menu);

                    if(p_menu.menu_type==="menu"){
                        var v_hijos = [];
                        p_uimenu.forEach(function(p_hijo){
                            if(p_hijo.parent_id===p_menu.id)
                                v_hijos.push(p_hijo);
                        });
                        console.log("DropdownSubMenu: ", p_menu);
                        return(
                            <DropdownSubMenu key={p_menu.id} name={p_menu.name}>
                                <DropdownMenu>
                                    <ViewUIMenu
                                        childrens={v_hijos}
                                        uimenu={p_uimenu}
                                        moduleId={p_moduleId}
                                        iteracion={p_iteracion+1}
                                        _handleOnClickApplication={p_onclick}
                                    />
                                </DropdownMenu>
                            </DropdownSubMenu>
                        )
                    }
                    else{
                        console.log("DropdownItem: ", p_menu);
                        return(
                            <DropdownItem key={p_menu.id} id={p_menu.id} name={p_menu.name} _handleOnClickApplication={p_onclick} />
                        )
                    }
                }
            )
            return(<>{elements}</>);
        }

        return(
            <ViewUIMenu
                childrens={this.props.childrens}
                uimenu={this.props.uimenu}
                moduleId={this.props.moduleId}
                iteracion={this.props.iteracion+1}
                _handleOnClickApplication={this.props._handleOnClickApplication}
            />
        )

    }
}