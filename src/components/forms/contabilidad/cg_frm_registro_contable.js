import { config } from '../../_constants';
import {authHeader, base64EncodeUnicode} from '../../_helpers';
//import axios from 'axios';
import React, {Component} from 'react';
//import ReactDOM from 'react-dom';
import $ from 'jquery';

import {ButtonSearchVoucherType} from '../querys/modalsearchvouchertype/buttonsearchvouchertype'
import {ButtonSearchAccount} from '../querys/modalsearchaccount/buttonsearchaccount'
import {MenuForm} from '../menuform/menuform'

export class CgFrmRegistroContable extends Component{

    constructor(props){ 
        super(props);

        this.state = {
            general: {numero: "", fecha: "", tipo_comprobante: "", glosa: "", total_debe: 0, total_haber: 0},
            detalle: [{entry_id: null, line: 1, id: null, code: '', name: '', debit: 0, credit: 0, rut: '', detalle: '', todas: false}],
            vouchertype: {},
            nuevo: false,
            uploadFileEntryDetailFile: null
        }

        this._setVoucherType = this._setVoucherType.bind(this);
        this._handleChangeAccountDetailAccount = this._handleChangeAccountDetailAccount.bind(this);
        this._handleChangeAccountDetailAccountInput = this._handleChangeAccountDetailAccountInput.bind(this);
        this._handleChangeAccountDetailDebit = this._handleChangeAccountDetailDebit.bind(this);
        this._handleChangeAccountDetailCredit = this._handleChangeAccountDetailCredit.bind(this);
        this._handleChangeAccountDetailRut = this._handleChangeAccountDetailRut.bind(this);
        this._handleChangeAccountDetailDetalle = this._handleChangeAccountDetailDetalle.bind(this);
        this._handleChangeAccountDetailTodas = this._handleChangeAccountDetailTodas.bind(this);
        this._handleOnClickBtnNuevo = this._handleOnClickBtnNuevo.bind(this);
        this._handleOnClickBtnGuardar = this._handleOnClickBtnGuardar.bind(this);
        this._handleOnClickBtnConsultar = this._handleOnClickBtnConsultar.bind(this);
        this._handleOnClickBtnAnular = this._handleOnClickBtnAnular.bind(this);
        this._handleOnClickBtnEliminar = this._handleOnClickBtnEliminar.bind(this);
        //this.getAccountDetailDetalleTodasTrue = this.getAccountDetailDetalleTodasTrue.bind(this);        

        this._handleClickImportEntryDetail = this._handleClickImportEntryDetail.bind(this);
        this._handleAcceptImportEntryDetail = this._handleAcceptImportEntryDetail.bind(this);
        this._handleCloseImportEntryDetail = this._handleCloseImportEntryDetail.bind(this);
        this._handleChangeUploadFileEntryDetailFile = this._handleChangeUploadFileEntryDetailFile.bind(this);
    } 

    getAccountDetailDetalleTodasTrue(){
        var v_detalle = this.state.detalle;
        var v_row = {rut: '', detalle: '', todas: false};
        v_detalle.map((row, index) => {
            if(v_detalle[index]["todas"]){
                v_row = v_detalle[index];
            }
        });
        return v_row;
    }
    
    duplicarAccountDetailDetalle(){
        var v_detalle = this.state.detalle;
        var v_source_index = -1;
        v_detalle.map((row, index) => {
            if(v_detalle[index]["todas"]){
                v_source_index = index;
            }
        });

        if(v_source_index>=0){ //existe algo que copiar
            var v_source = v_detalle[v_source_index];
            v_detalle.map((row, index) => {
                if(index!=v_source_index){
                    v_detalle[index]["rut"] = v_source["rut"];
                    v_detalle[index]["detalle"] = v_source["detalle"];
                    v_detalle[index]["todas"] = false;
                }
            });
            this.setState({detalle: v_detalle});
        }
    }
    
    _setVoucherType(p_data){
        this.setState(
            {
                vouchertype: {id: p_data.id, company_id: p_data.company_id, voucher_type_code: p_data.voucher_type_code
                            , name: p_data.name, description: p_data.description, class_type: p_data.class_type},
            }
        )
        this.setState({general: {tipo_comprobante: p_data.voucher_type_code}});
    }
    
    _handleChangeNumero(event) {
        this.setState({general: {numero: event.target.value}});
    }

    _handleChangeFecha(event) {
        this.setState({general: {fecha: event.target.value}});
    }

    _handleChangeTipoComprobante(event) {
        this.setState({general: {tipo_comprobante: event.target.value}});
    }

    _handleChangeGlosa(event) {
        this.setState({general: {glosa: event.target.value}});
    }

    _handleClickAddRow(event){
        var v_detalle = this.state.detalle;
        var v_line = v_detalle.length;
        var v_row_duplicar_detalle = {rut: '', detalle: '', todas: false};
        if(v_line===0) v_line=1
        else v_line++;
        
        v_row_duplicar_detalle = this.getAccountDetailDetalleTodasTrue();
        v_detalle.push({entry_id: null, line: v_line, id: null, code: '', name: '', debit: 0, credit: 0
                        , rut: v_row_duplicar_detalle.rut, detalle: v_row_duplicar_detalle.detalle, todas: false})
        
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailAccount(p_line, p_id, p_code, p_name) {
        var v_detalle = this.state.detalle;
        //v_detalle[p_line]["account_id"] = p_value;
        v_detalle[p_line]["id"] = p_id;
        v_detalle[p_line]["code"] = p_code;
        v_detalle[p_line]["name"] = p_name;

        //console.log("p_line",p_line,p_id,p_code,p_name);
        //console.log("v_detalle",v_detalle);
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailAccountInput(p_line, p_code) {
        /*
        let v_id = null;
        let v_name = null;

        let v_detalle = this.state.detalle;
        //v_detalle[p_line]["account_id"] = p_value;
        v_detalle[p_line]["id"] = p_id;
        v_detalle[p_line]["code"] = p_code;
        v_detalle[p_line]["name"] = p_name;

        console.log("p_line",p_line,p_id,p_code,p_name);
        console.log("v_detalle",v_detalle);
        this.setState({detalle: v_detalle});
        */
    }

    _handleChangeAccountDetailDebit(p_line, p_value) {
        var v_detalle = this.state.detalle;
        v_detalle[p_line]["debit"] = p_value;
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailCredit(p_line, p_value) {
        var v_detalle = this.state.detalle;
        v_detalle[p_line]["credit"] = p_value;
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailRut(p_line, p_value) {
        //console.log("principal->_handleChangeAccountDetailRut",p_line,p_value);
        var v_detalle = this.state.detalle;
        v_detalle[p_line]["rut"] = p_value;
        this.setState({detalle: v_detalle});
        //console.log(this.state.detalle);
    }

    _handleChangeAccountDetailDetalle(p_line, p_value) {
        var v_detalle = this.state.detalle;
        v_detalle[p_line]["detalle"] = p_value;
        this.setState({detalle: v_detalle});
    }

    _handleChangeAccountDetailTodas(p_line, p_value) {
        var v_detalle = this.state.detalle;
        var v_existeOtraLineaConTodasTrue = false;
        if(p_value){
            //validar que no exista otra linea con todas = true  
            v_detalle.map((row, index) => {
                if(index!=p_line && v_detalle[index]["todas"]==true){
                    v_existeOtraLineaConTodasTrue = true;
                }
            });  
        }
        if(v_existeOtraLineaConTodasTrue){
            alert("Nos es posible realizar el cambio, ya que existe otra línea marca para repetir");
            return false;
        }
        else{
            v_detalle[p_line]["todas"] = p_value;
            this.setState({detalle: v_detalle});
        }
    }

    _handleClickCopyRow(p_index) {
        var v_detalle = this.state.detalle;
        v_detalle.push({entry_id: v_detalle[p_index].entry_id, line: v_detalle[p_index].line, account_id: v_detalle[p_index].account_id, debit: v_detalle[p_index].debit, credit: v_detalle[p_index].credit, rut: v_detalle[p_index].rut, detalle: v_detalle[p_index].detalle, todas: false})
        this.setState({detalle: v_detalle});
    }

    _handleClickDelRow(p_index) {
        var v_detalle = this.state.detalle;
        v_detalle.splice(p_index, 1);
        this.setState({detalle: v_detalle});
    }

    _handleClickImportEntryDetail(){
        
        $(document.getElementById("uploadFileEntryDetail")).modal("show");
    }
    
    _handleAcceptImportEntryDetail(){     
        let user = JSON.parse(localStorage.getItem('user'));
        let company = JSON.parse(localStorage.getItem('company'));
        let v_company = base64EncodeUnicode(company.id);
        let v_route = base64EncodeUnicode("accounting");
        let v_option = base64EncodeUnicode("1");
        console.log('-> '+company.id)
        const formData = new FormData();
        formData.append( 
            "fileEntryDetail", 
            this.state.uploadFileEntryDetailFile, 
            this.state.uploadFileEntryDetailFile.name
        );
        
        const requestOptions = { 
            method: 'POST',
            //headers: authHeader(),
            body: formData //JSON.stringify({ title: 'React POST Request Example' })
        };

        fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_company}`, requestOptions)
            .then(async response => {
                const data = await response.json();
                console.log("data",data);
            })
			.catch((error) => {
				console.error('Error:', error);
			});
            
        $(document.getElementById("uploadFileEntryDetail")).modal("hide")
    }
  
    _handleCloseImportEntryDetail(){
        $(document.getElementById("uploadFileEntryDetail")).modal("hide")
    }
    
    _handleChangeUploadFileEntryDetailFile(e){ 
        // Update the state 
        this.setState({ uploadFileEntryDetailFile: e.target.files[0] }); 
    };

    _handleOnClickBtnNuevo(){
        this.setState({nuevo: true});
    }

    _handleOnClickBtnGuardar(){
        //this.setState({nuevo: false});
    }

    _handleOnClickBtnConsultar(){
        console.log("Consultar");
    }

    _handleOnClickBtnAnular(){
        console.log("Anular");
    }

    _handleOnClickBtnEliminar(){
        console.log("Eliminar");
    }
    
    render(){
        const menuitems = [ {type: 'NUEVO', onClick: this._handleOnClickBtnNuevo},
                            {type: 'GUARDAR', onClick: this._handleOnClickBtnGuardar},
                            {type: 'CONSULTAR', onClick: this._handleOnClickBtnConsultar},
                            {type: 'ANULAR', onClick: this._handleOnClickBtnAnular},
                            {type: 'ELIMINAR', onClick: this._handleOnClickBtnEliminar}];
        return(
        <>
            <MenuForm menuitems={menuitems} />
            <div className="row">
                <div className="col-lg-12">
                <div className="card">
                    <div className="card-header">
                        <strong>Informaci&oacute;n General</strong>
                    </div>
                    <div className="card-body">                    
                        <form>
                            <div className="form-group row">
                                <label htmlFor="inputNumero" 
                                    className="col-md-2 col-lg-1 col-form-label col-form-label-xs">
                                        N&uacute;mero
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <input className="form-control form-control-xs" 
                                        id="inputNumero" 
                                        type="text" 
                                        placeholder=""
                                        value={this.state.general.numero}
                                        onChange={this._handleChangeNumero.bind(this)} 
                                        readOnly={this.state.nuevo}
                                    />
                                </div>
                                <label htmlFor="inputUsuario" 
                                    className="col-md-2 col-lg-2 col-form-label col-form-label-xs">
                                        Usuario
                                </label>
                                <div className="col-md-7 col-lg-7">
                                    <input type="text" 
                                        className="form-control-plaintext form-control-xs" 
                                        readOnly
                                        id="inputUsuario" 
                                        value={this.props.authentication.user.name}
                                    />
                                </div>
                                
                            </div> 
                            <div className="form-group row">
                                <label htmlFor="inputFecha" 
                                    className="col-md-2 col-lg-1 col-form-label col-form-label-xs">
                                    Fecha
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <input className="form-control form-control-xs" 
                                        id="inputFecha" 
                                        type="text" 
                                        placeholder="dd/mm/yyyy"
                                        value={this.state.general.fecha}
                                        onChange={this._handleChangeFecha.bind(this)} 
                                    />
                                </div>
                                <label htmlFor="inputTipoComprobante" 
                                    className="col-md-2 col-lg-2 col-form-label col-form-label-xs">
                                    Tipo Comprobante
                                </label>
                                <div className="col-md-2 col-lg-2">
                                    <div className="input-group input-group-xs">
                                        <input className="form-control py-1" 
                                            id="inputTipoComprobante" 
                                            type="text" 
                                            placeholder=""
                                            value={this.state.general.tipo_comprobante}
                                            onChange={this._handleChangeTipoComprobante.bind(this)}   
                                        />
                                        <div className="input-group-append">
                                            <ButtonSearchVoucherType 
                                                modalId="cnVoucherType"
                                                setValue={this._setVoucherType}/>
                                        </div>
                                    </div>   
                                </div>                         
                            </div> 
                            <div className="form-group row">
                                <label htmlFor="inputGlosa" 
                                    className="col-md-2 col-lg-1 col-form-label col-form-label-xs">
                                    Glosa
                                </label>
                                <div className="col-md-10 col-lg-11">
                                    <textarea className="form-control form-control-xs" 
                                        id="inputGlosa" 
                                        type="text" 
                                        placeholder=""
                                        value={this.state.general.glosa}
                                        onChange={this._handleChangeGlosa.bind(this)} 
                                    >
                                    </textarea>
                                </div>                        
                            </div> 
                        </form>
                    </div>
                </div>
                </div>
            </div>
            <br />    
            <button className="btn btn-outline-info btn-xs" type="button" onClick={this._handleClickImportEntryDetail.bind(this)}>
                <i className="fas fa-file-upload"></i>&nbsp;Importar Detalle
            </button> 
            <div className="row">
                <div className="col-lg-12">
                <div className="card">
                    <div className="card-header d-flex justify-content-between align-items-center">
                        <strong>Detalle</strong>&nbsp;
                        <button className="btn btn-outline-info btn-xs" type="button" onClick={this._handleClickAddRow.bind(this)}>
                            <i className="fas fa-plus"></i>&nbsp;Agregar
                        </button>     
                    </div>
                    <div className="card-body">
                        <table className="table table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th scope="col" style={{width: '50px', textAlign: 'center'}}>L&iacute;nea</th>
                                    <th scope="col" colSpan="2">Cuenta</th>
                                    <th scope="col" style={{width: '130px', textAlign: 'center'}}>Debe</th>
                                    <th scope="col" style={{width: '130px', textAlign: 'center'}}>Haber</th>
                                    <th scope="col" style={{width: '150px', textAlign: 'center'}}>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.detalle.map((row, index) => {
                                        return(  
                                            <AccountDetail 
                                                key={index}
                                                index={index}
                                                line={row.line} 
                                                account_id={row.code}
                                                account_name={row.name}
                                                debit={row.debit}
                                                credit={row.credit}
                                                rut={row.rut}
                                                detalle={row.detalle}
                                                todas={row.todas}
                                                onChangeAccountId={this._handleChangeAccountDetailAccount}
                                                onChangeAccountIdInput={this._handleChangeAccountDetailAccountInput}
                                                onChangeDebit={this._handleChangeAccountDetailDebit}
                                                onChangeCredit={this._handleChangeAccountDetailCredit}
                                                onClickCopyRow={this._handleClickCopyRow.bind(this)}
                                                onClickDelRow={this._handleClickDelRow.bind(this)}
                                                onChangeRut={this._handleChangeAccountDetailRut}
                                                onChangeDetalle={this._handleChangeAccountDetailDetalle}
                                                onChangeTodas={this._handleChangeAccountDetailTodas}
                                            />
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
            
            <div className="modal fade" tabIndex="-1" role="dialog" id="uploadFileEntryDetail">
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Importar Archivo</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">                                       
                        <form>
                            <div className="form-group row">
                                <input type="file" 
                                    className="form-control-file" 
                                    id="uploadFileEntryDetail_file" 
                                    onChange={this._handleChangeUploadFileEntryDetailFile}
                                />
                            </div>
                        </form>
                        <div className="row">
                            <p>
                                <br />
                                <strong>Caracteristicas del archivo:</strong>
                                <ul>
                                    <li>Tipo de archivo Excel (xlsx)</li>
                                    <li>No debe tener encabezado</li>
                                    <li>Columnas (deben estar en el mismo orden que se indica a continuaci&oacute;n</li>
                                    <ol>
                                        <li>N&uacute;mero de cuenta (num&eacute;rico - obligatorio)</li>
                                        <li>(*) Monto al debe (num&eacute;rico - obligatorio)</li>
                                        <li>(*) Monto al haber (num&eacute;rico - obligatorio)</li>
                                        <li>Rut con gui&oacute;n y digito verificador (alfanum&eacute;rico - no obligatorio)</li>
                                        <li>Texto con el detalle asociado a la l&iacute;nea (alfanum&eacute;rico - no obligatorio)</li>
                                    </ol>
                                </ul>
                                * Si existe un valor distinto de cero al Debe y Haber se crearán dos l&iacute;neas contables.
                            </p>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={this._handleAcceptImportEntryDetail}>Importar</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this._handleCloseImportEntryDetail}>Cancelar</button>
                    </div>
                    </div>
                </div>
            </div>             
        </>
      );
    }
}
  
function AccountDetail(props){    

    function _setAccount(p_data){
        //console.log("p_data",p_data)
        if(p_data.isLeaf){
            props.onChangeAccountId(props.index, p_data.id, p_data.account_code,p_data.name);
        }
    }

    function _handleChangeAccountId(e){
        //console.log("props.index",props.index);
        props.onChangeAccountIdInput(props.index, e.target.value);
    }

    function _handleChangeDebit(e){
        props.onChangeDebit(props.index, e.target.value);
    }

    function _handleChangeCredit(e){
        props.onChangeCredit(props.index, e.target.value);
    }

    function _handleAcceptAccDetailLine(){
        
        $(document.getElementById(props.modalId)).modal("hide")
    }
  
    function _handleCloseAccDetailLine(){
        $(document.getElementById(props.modalId)).modal("hide")
    }

    function _handleChangeRut(e){
        //console.log("AccountDetail->_handleChangeRut",props.index,e.target.value);
        props.onChangeRut(props.index,e.target.value);
    }

    function _handleChangeDetalle(e){
        props.onChangeDetalle(props.index,e.target.value);
    }

    function _handleChangeTodas(e){
        props.onChangeTodas(props.index,e.target.checked);
    }

    function _handleClickEditRow(e){
        $(document.getElementById("accDetailLine"+props.index)).modal("show")
    }

    function _handleClickCopyRow(e){
        props.onClickCopyRow(props.index);
    }

    function _handleClickDelRow(e){
        props.onClickDelRow(props.index);
    }

    return(
        <tr>
            <th scope="row">{props.line}</th>
            <td style={{width: '150px',}}>
                <div className="input-group input-group-xs">
                    <input className="form-control py-1" 
                        id={"inputAccountId"+props.index} 
                        type="text" 
                        placeholder=""
                        value={props.account_id}
                        onChange={_handleChangeAccountId}
                    />
                    <div className="input-group-append">
                        <ButtonSearchAccount 
                            modalId="cnAccount"
                            setValue={_setAccount}/>
                    </div>
                </div> 
            </td>
            <td>
                {props.account_name}
            </td>
            <td>
                <input className="form-control py-1 form-control-xs" 
                        id={"inputDebit"+props.index}
                        type="text" 
                        placeholder=""
                        style={{textAlign: 'right'}}
                        value={props.debit}
                        onChange={_handleChangeDebit}
                />
            </td>
            <td>
                <input className="form-control py-1 form-control-xs" 
                        id={"inputCredit"+props.index}
                        type="text" 
                        placeholder=""
                        style={{textAlign: 'right'}}
                        value={props.credit}
                        onChange={_handleChangeCredit}
                />
            </td>
            <td>
                <button className="btn btn-outline-primary btn-xs" type="button" onClick={_handleClickEditRow}>
                    <i className="fas fa-paperclip"></i>
                </button>&nbsp;
                <button className="btn btn-outline-info btn-xs" type="button" onClick={_handleClickCopyRow}>
                    <i className="fas fa-copy"></i>
                </button>&nbsp;
                <button className="btn btn-outline-danger btn-xs" type="button" onClick={_handleClickDelRow}>
                    <i className="fas fa-trash"></i>
                </button>
                <div className="modal fade" tabIndex="-1" role="dialog" id={"accDetailLine"+props.index}>
                    <div className="modal-dialog modal-md" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Detalle</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">                                       
                            <form>
                                <div className="form-group row">
                                    <label htmlFor={"inputDetailRut"+props.index}
                                        className="col-md-3 col-lg-3 col-form-label col-form-label-xs">
                                            Rut
                                    </label>
                                    <div className="col-md-3 col-lg-3">
                                        <input className="form-control form-control-xs" 
                                            id={"inputDetailRut"+props.index}
                                            type="text" 
                                            placeholder=""
                                            value={props.rut}
                                            onChange={_handleChangeRut} 
                                        />
                                    </div>                            
                                </div> 
                                <div className="form-group row">
                                    <label htmlFor={"inputDetailDetalle"+props.index}
                                        className="col-md-3 col-lg-3 col-form-label col-form-label-xs">
                                        Descripci&oacute;n
                                    </label>
                                    <div className="col-md-9 col-lg-9">
                                        <textarea className="form-control form-control-xs" 
                                            id={"inputDetailDetalle"+props.index}
                                            type="text" 
                                            placeholder=""
                                            value={props.detalle}
                                            onChange={_handleChangeDetalle} 
                                        >
                                        </textarea>
                                    </div>                        
                                </div> 
                                <div className="form-check row">
                                    <input className="form-check-input" 
                                        id={"inputDetailTodas"+props.index}
                                        type="checkbox" 
                                        checked={props.todas}
                                        onChange={_handleChangeTodas} 
                                    />
                                    <label className="form-check-label" 
                                        htmlFor={"inputDetailTodas"+props.index}
                                    >
                                        Repetir en todas las l&iacute;neas nuevas
                                    </label>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={_handleAcceptAccDetailLine}>Aceptar</button>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={_handleCloseAccDetailLine}>Cerrar</button>
                        </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    )
}