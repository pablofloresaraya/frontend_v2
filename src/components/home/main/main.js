import React, {Component} from 'react';
import { connect } from 'react-redux';
import { menuActions } from '../../_actions';

import {UIMenu} from './uimenu/uimenu';
import {Applications} from './applications/applications' 
//import { ClinicaFormsRoute } from '../../forms/clinica';
import { ContabilidadFormsRoute } from '../../forms/contabilidad';

class Main extends Component{
    constructor(props){
      super(props);

      this.state =  {
                    appTabs: [{id: 0, name: "Inicio"},
                              {id: 1, name: "Clientes y Proveedores"},
                              {id: 2, name: "Libro Diario"},
                              {id: 3, name: "Libro Mayor"}], 
                    applicationActive: 0,
                    //appForms: [{id: 0, content: ClinicaFormsRoute.show(0)}],
                    appForms: [
                             {id: 0, content: ContabilidadFormsRoute.show(0,this.props.authentication)}
                            ,{id: 1, content: ContabilidadFormsRoute.show(1,this.props.authentication)}
                            ,{id: 2, content: ContabilidadFormsRoute.show(2,this.props.authentication)}
                            ,{id: 3, content: ContabilidadFormsRoute.show(3,this.props.authentication)}  
                          ]
                    };

      this.addApplicationTab = this.addApplicationTab.bind(this);
      this._handleClickTab = this._handleClickTab.bind(this);
    }

    addApplicationTab(app){
      const app_tabs = this.state.appTabs;
      const app_forms = this.state.appForms;

      const pos = app_tabs.map(function(e) { return e.id; }).indexOf(app.id);

      if(pos===-1){
        app_tabs.push({id: app.id, name: app.name});
        //app_forms.push({id: app.id, content: ClinicaFormsRoute.show(app.id)});
        console.log("FORM_CONTABILIDAD: "+app.id)
        console.log("this.props.authentication: ",this.props.authentication)
        app_forms.push({id: app.id, content: ContabilidadFormsRoute.show(app.id,this.props.authentication)});
      }

      this.setState({appTabs: app_tabs, applicationActive: app.id, appForms: app_forms});
    }

    _handleClickTab(id){
      this.setState({applicationActive: id});
    }

    render(){
      console.log("3. Main props: ",this.props);
      return(
        <main>
          <UIMenu
                addApplicationTab={this.addApplicationTab}
                moduleUiMenu={this.props.menu.moduleUiMenu}
                moduleSections={this.props.menu.moduleSections}
                menuModuleActive={this.props.menu.menuModuleActive}
                menuNameModuleActive={this.props.menu.menuNameModuleActive}
          />
          <Applications
                appTabs={this.state.appTabs}
                applicationActive={this.state.applicationActive}
                appForms={this.state.appForms}
                _handleClickTab={this._handleClickTab}
          />
        </main>
      );
    }
  }

  function mapStateToProps(state) {
      const menu = state.menu;
      const authentication = state.authentication;
      return {menu,authentication};
  }
  
  const mapDispatchToProps = {
    setModuleUIMenu: menuActions.setModuleUIMenu
  };
  
  const connectMain = connect(mapStateToProps, mapDispatchToProps)(Main);
  export { connectMain as Main };