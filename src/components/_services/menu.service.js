import { config } from '../_constants';
import { authHeader, base64EncodeUnicode } from '../_helpers';
import axios from 'axios';

export const menuService = {
    getModuleSections,
    getModuleApplications,
    getModuleUiMenu
};

function getModuleUiMenu(p_module_id) {  

    let user = JSON.parse(localStorage.getItem('user'));
    var v_route = base64EncodeUnicode("module");
    var v_option = base64EncodeUnicode("1");
    
    const formData = new FormData();
    formData.append( "var1", p_module_id);

    const requestOptions = {
        method: 'POST',
        //headers: authHeader(),
        //headers: {'Authorization': 'Bearer ' + user.token},
        body: formData
        //body: JSON.stringify({ t: user.token, r: v_route, o: v_option, var1: p_module_id })
    };

    return fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_option}`, requestOptions)
        .then(handleResponse)
        .then(menus => {
            return menus.data;
        });
}

function getModuleSections(p_module_id) {   
    let user = JSON.parse(localStorage.getItem('user'));
    var v_route = base64EncodeUnicode("module");
    var v_option = base64EncodeUnicode("2");

    const formData = new FormData();
    formData.append( "var1", p_module_id);

    const requestOptions = {
        method: 'POST',
        //headers: authHeader(),
        body: formData
    };

    return fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_option}`, requestOptions)
        .then(handleResponse)
        .then(sections => {
            return sections.data;
        });
}

function getModuleApplications(p_module_id, p_section_id) { 
    let user = JSON.parse(localStorage.getItem('user'));
    var v_route = base64EncodeUnicode("module");
    var v_option = base64EncodeUnicode("3");

    const formData = new FormData();
    formData.append( "var1", p_module_id);
    formData.append( "var2", p_section_id);

    const requestOptions = {
        method: 'POST',
        //headers: authHeader(),
        body: formData
    };

    return fetch(`${config.apiUrl}/index.php?t=${user.token}&r=${v_route}&o=${v_option}&c=${v_option}`, requestOptions)
        .then(handleResponse)
        .then(applications => {
            return applications.data;
        });
}

/*
function getModuleUiMenu(p_module_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/modules/${p_module_id}/all`, requestOptions)
        .then(handleResponse)
        .then(menus => {
            return menus.data;
        });
}

function getModuleSections(p_module_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/modules/${p_module_id}/sections`, requestOptions)
        .then(handleResponse)
        .then(sections => {
            return sections.data;
        });
}

function getModuleApplications(p_module_id, p_section_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/modules/applications?module_id=${p_module_id}&section_id=${p_section_id}`, requestOptions)
        .then(handleResponse)
        .then(applications => {
            return applications;
        });
}
*/

function handleResponse(response) {
    //console.log("response",response);
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        //console.log("data",data);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                console.log("No fue posible obtener las aplicaciones existentes en el modulo");
            }

            const error = (data && data.message) || response.statusText;
            console.log("error:", error);

            return Promise.reject(error);
        }

        return data;
    });
}