import React, {Component} from 'react';
import { menuService } from '../../../_services';
import $ from 'jquery';


function NavbarNav(props){

    return(
      <ul className="navbar-nav ml-auto">
        {props.children}
      </ul>
    );
}

function NavItem(props){
  return(
      <li className="nav-item dropdown">{ props.children }</li>
  )
}

function NavLink(props){
  return(
    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      {props.name}
    </a>
  )
}

function DropdownMenu(props){
  return(
      <div className="dropdown-menu" aria-labelledby="navbarDropdown">{ props.children }</div>
  )
}

function DropdownItem(props){

  return(
      <a className={"dropdown-item "+props.class} href="#">{props.name}</a>
  )

}

function DropdownSubMenu(props){
  //<a class="dropdown-item dropdown-toggle" href="#">{props.name}</a>
  return (
    <div className="dropdown-submenu">
      <a className="dropdown-item dropdown-toggle" href="#">{props.name}</a>
      {props.children}
    </div>
  )
}


function ViewUIMenu(props){
    const p_uimenu = props.uimenu;
    const p_moduleId = props.moduleId;
    
    let v_hijos = [];
    p_uimenu.forEach(
        function(p_menu,p_index,p_arr_uimenu){
            if(p_menu.menu_type==="menu"){
                p_arr_uimenu.forEach(function(p_hijo){
                    if(p_hijo.parent_id===p_menu.id)
                        v_hijos.push(p_hijo);
                });
                if(parseInt(p_moduleId)===p_menu.parent_id){
                    return(
                        <NavItem>
                            <NavLink name={p_menu.name} />
                            <DropdownMenu>
                                <ViewUIMenu uimenu={v_hijos} />
                            </DropdownMenu>
                        </NavItem>
                    )
                }
                else{
                    return(
                        <DropdownSubMenu name={p_menu.name}>
                            <DropdownMenu>
                                <ViewUIMenu uimenu={v_hijos} />
                            </DropdownMenu>
                        </DropdownSubMenu>
                    )
                }
            }
            else{
                return(
                    <DropdownItem name={p_menu.name} />
                )
            }
        }
    )
    return(
        <div></div>
    )
}

function ShowUIMenu(props){
    //p_moduleId = this.props.moduleActive;
    const p_moduleId = props.moduleActive;
    const p_uimenu = props.moduleUiMenu;
    
    let uimenu = p_uimenu;
    console.log('uimenu: ', uimenu);
    if(uimenu.length==0){
        return(<NavbarNav></NavbarNav>);
    }
    else{
        return(
            <NavbarNav>
                <ViewUIMenu 
                    uimenu={uimenu}
                    moduleId={p_moduleId}
                />
            </NavbarNav>
        );
    }
}

export class UIMenuView extends Component{
    constructor(props) {
        super(props);
        this.state = {uimenu: []};

        this._handleOnClickApplication = this._handleOnClickApplication.bind(this);
    }

    componentDidMount(){

        $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
            }
            var $subMenu = $(this).next(".dropdown-menu");
            $subMenu.toggleClass('show');

            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
                $('.dropdown-submenu .show').removeClass("show");
            });

            return false;
        });
    }

    _handleOnClickApplication(app){
        this.props.addApplicationTab(app);
    }

    render(){
      console.log("4. Main UIMenu props", this.props);

      return(
        <nav  className="navbar navbar-expand-lg navbar-light bg-light"
              style={{ paddingTop: '1px', paddingBottom: '1px', paddingRight: '10rem'}}
        >
          <a className="navbar-brand" href="#"
              style={{ color: 'rgba(162, 157, 157, 0.5)', fontSize: '1.5rem', padding: '0px'}}
          >
              <strong>{this.props.moduleName}</strong>
          </a>
          <button
                  className="navbar-toggler navbar-toggler-right"
                  type="button"
                  data-toggle="collapse"
                  data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ShowUIMenu
                moduleActive={this.props.moduleActive }
                moduleUiMenu={this.props.moduleUiMenu}
            />
          </div>
      </nav>
      );
    }
  }