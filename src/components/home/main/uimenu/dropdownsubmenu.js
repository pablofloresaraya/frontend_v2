import React from 'react';

export function DropdownSubMenu(props){ 
  //<a class="dropdown-item dropdown-toggle" href="#">{props.name}</a>
  return (
    <div className="dropdown-submenu">
      <a className="dropdown-item dropdown-toggle" href="/#">{props.name}</a>
      {props.children}
    </div>
  )
}