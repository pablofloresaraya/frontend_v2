import React from 'react';

import {ProvincesTypeList} from './ProvincesTypeList'; 

export function ModalSearchProvinceType(props){
    
    return(
        <div className="modal" tabIndex="-1" role="dialog" id={props.id}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Seleccione Provincia</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <ProvincesTypeList                              
                            setValue={props.setValue}
                            region={props.region}
                            close={props.close}
                        />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={props.close}>Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    );
    
}

export default ModalSearchProvinceType;