import React from 'react';

export function NavbarNav(props){
    return(
      <ul className="navbar-nav ml-auto">
        {props.children}
      </ul>
    );
}