import React, {Component} from 'react';
import {Form} from './form'

export class ApplicationsForms extends Component{
    /*
    constructor(props){
        super(props);
    }
    */
    render(){
        return(
            <div className="tab-content" id="myTabAppOpenContent" style={{height: '30rem',margin: '10px'}}>
                {
                    this.props.appForms.map(
                        (app) => <Form key={app.id} id={app.id} content={app.content} applicationActive={this.props.applicationActive} />
                    )
                }
            </div>
      );
    }
  }